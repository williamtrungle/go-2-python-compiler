package main
func main () {
    _ := 3+4
    _, _ := 5, 6
    a, _ := 1, 2
    var _ int
    var _ int = 3
    var _ = 3
    var _, b = 3, 3
    var _, c int = 3, 3
    var _, e int
    var (
        _ int
        _ int =3
        _ = 3
        a, _ = 3, 3
        a, _ int
        a, _ int = 3 ,3
    )
    type _ int
    type a struct {
        _ int
    }
}
