package main
func main() {
    a &^= 1
}
    /*
    a = 1
    _ = 1
    a += 1
    a -= 1
    a *= 1
    a /= 1
    a %= 1
    a |= 1
    a &= 1
    a ^= 1
    a <<= 1
    a >>= 1
    (a) = 2 + 2
}
    */
