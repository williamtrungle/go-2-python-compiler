package main
func foo () [4]int {}
func bar (a [4]int) {}
func main () {
    x := foo()[5]
    y = bar()[3]
    z = main().member
    a = (main())
    a = (main())[5]
    a = (main()).member
    a = a[2][34][1][2]
    a = main()[2][3][5]
    a = (a[0])[0]
    a = ((a[0])[0])[0]
    a = ((a[0])[0])
    a = (a)[2][3][4]
    a = (main())[2][3][4]
    a = (main())[0][0].a.a[0]
}
