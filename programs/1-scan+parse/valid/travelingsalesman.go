package main

var MAX_INT = 4294967295
var CONST_SIZE = 10
var CONST_DIST = 1000
var CONST_PROB = 1 // Leave at fully connected to ensure all nodes reachable

var _m = 2147483648
var _a = 1103515245
var _c = 12345
var _r int

func seed(s int) {
    _r = s
}

func rand(high int) int {
    randomint := (_a * _r + _c) % _m
    _r = randomint
    return randomint % high
}

type Node struct {
    distance []int
}

func NewNode(maxn int) Node {
    var node Node
    //node := Node{distance: make([]int, maxn)}
    for i := 0; i < maxn; i++ {
        node.distance[i] = 0
    }
    return node
}

type Graph struct {
    nodes []Node
}

func NewGraph(maxn int) Graph {
    var graph Graph
    //graph := Graph{nodes: make([]Node, maxn)}
    for i := 0; i < maxn; i++ {
        graph.nodes[i] = NewNode(maxn)
    }
    return graph
}

func connect(g Graph, d, p int) {
    l := len(g.nodes)
    var distance int
    for i := 0; i < l; i++ {
        this := g.nodes[i]
        for j := (i+1) % l ; j != i; j = (j+1) % l {
            that := g.nodes[j]
            if (rand(d) % p) == 0 {
                distance = rand(d) + 1 // no null distance
                this.distance[j] = distance
                that.distance[i] = distance
            } else {
                this.distance[j] = MAX_INT
                that.distance[i] = MAX_INT
            }
        }
    }
}

func travel(g Graph) []int {
    l := len(g.nodes)
    var path, r []int
    //path := make([]int, l)
    //r := make([]int, l)
    for i := 0; i < l; i++ {
        path[i] = i
        r[i] = i
    }
    distance := length(g, path)
    distance = permutate(path, r, 0, l-1, distance, g)
    return r
}

func permutate(p, r []int, start, end, distance int, g Graph) int {
    if start == end {
        if length(g, p) < distance {
            deepCopy(p, r)
            return length(g, p)
        }
    } else {
        d := distance
        for i := start; i < end+1; i++ {
            t := p[start]
            p[start] = p[i]
            p[i] = t
            d = permutate(p, r, start+1, end, d, g)
            t = p[start]
            p[start] = p[i]
            p[i] = t
        }
        return d
    }
    return distance
}

func length(g Graph, path []int) int {
    sum := 0
    for i := 1; i < len(path); i++ {
        sum += g.nodes[path[i]].distance[path[i-1]]
    }
    return sum
}

func deepCopy(a, b []int) {
    for i := 0; i < len(a); i++ {
        b[i] = a[i]
    }
}

func printGraph(g Graph) {
    l := len(g.nodes)
    print("   ")
    for i := 0; i < l; i++ {
        print("%d\t", i)
    }
    print("\n")
    for i := 0; i < l; i++ {
        print("%d  ", i)
        for j := 0; j < l; j++ {
            d := g.nodes[i].distance[j]
            if i != j && d != MAX_INT {
                print("%d\t", d)
            } else {
                print("-\t")
            }
        }
        println()
    }
}


func main() {
    seed(3)
    numNodes := CONST_SIZE
    maxDist := CONST_DIST
    connectProb := CONST_PROB
    graph := NewGraph(numNodes)

    print("Graph: %d nodes\n",numNodes)
    print("Max distance between nodes: %d\n",maxDist)
    connect(graph, maxDist, connectProb)
    printGraph(graph)
    println("Computing optimal path...")
    path := travel(graph)
    for i := 0; i < len(path); i++ {
        if i == 0 {
            print("%d ", path[i])
        } else {
            print("-> %d ", path[i])
        }
    }
    print("\nShortest path: %d\n", length(graph, path))
}
