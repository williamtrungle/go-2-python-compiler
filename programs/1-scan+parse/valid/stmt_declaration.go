package main
func main(){
    var a int
    var b = 1
    var c int = 2
    var a,b,_ int
    var (
        a int
        b = 1
        c int = 2
        a,b,c int
        a,b,c = 1,2,3
        a,b,c int = 1,2,3
        a struct {
            a int
            b []int
            c [3]int
            d struct {
                a int
            }
        }
    )
    type _ int
    type a int
    type a []int
    type b [3]int
    type c struct {
        a int
    }
    type (
        a int
        a []int
        b [3]int
        c struct {
            a int
        }
    )
}
