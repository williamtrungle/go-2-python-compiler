package main
func main() {
    if true {
        a := 1
    } else if 0 > 10 {
        print()
    } else {
        return
    }
    println()
    if z := 10; z > 10 {
        a,b := 20, z + 1
        if !foo() {
            print("bar")
        } else {
            return
        }
    }
}
