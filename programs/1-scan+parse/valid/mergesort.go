package sort

var _m = 2147483648
var _a = 1103515245
var _c = 12345
var _r int

func seed(s int) {
    _r = s
}

func rand(high int) int {
    randomint := (_a * _r + _c) % _m
    _r = randomint
    return randomint % high
}

func Merge(list []int) []int {
    if len(list) == 1 {
        return list
    } else {
        midpoint := len(list) / 2
        var lower, upper []int
        for i := 0; i < midpoint; i++ {
            lower[i] = list[i]
        }
        for i := midpoint; i < len(list); i++ {
            upper[i] = list[i]
        }
        return mergeHelper(lower, upper)
    }
}

func mergeHelper(lower, upper []int) []int {
    length, a, b := len(lower) + len(upper), 0, 0
    var list []int

    for i := 0; i < length; i++ {
        if a > len(lower) - 1 && b <= len(upper) - 1 {
            list[i] = upper[b]
            b++
        } else if b > len(upper) - 1 && a <= len(lower) - 1 {
            list[i] = lower[a]
            a++
        } else if lower[a] < upper[b] {
            list[i] = lower[a]
            a++
        } else {
            list[i] = upper[b]
            b++
        }
    }
    return list
}

func main() {
    seed(42)
    size := 1000
    var list []int
    for i := 0; i < size; i++ {
        list[i] = rand(1000000);
    }
    sorted := Merge(list)
    for i := 0; i < size; i++ {
        print(list[i])
    }
}
