package main 

func main() {
	generatePrimes(1000)
}

func generatePrimes(maxx int) {
	for i := 1; i <= maxx; i++ {
		valid := true
		for j := i-1; valid && j >= 2; j-- {
			if (i % j == 0) {
				valid = false
			}
		}
		if (valid) {
			println(i)
		}
	}
}