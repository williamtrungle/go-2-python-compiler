package main
type (
    a int
    b string
)
type (
    c struct {
        q int
    }
    d struct {
        w, e string
        b int
    }
    e struct { a int; }
)
type (
    num int
    point struct {
        x, y float64

    }
)
