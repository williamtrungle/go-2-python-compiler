package main


// higher-order function that combines a series defined by
// the number of iterations of the series to calculate, a way to
// calculate the next element of the series, a way to combine
// previous calculated values with the next value, and
// a root value to start with and a neutral value.
func combineSeries(iters int, seriesCalc int,
	combine int,
	increment int,
	root int, neutral int) int {
	i := 0
	current := root

	total := neutral
	for i < iters {
		total = combine(seriesCalc(current), total)
		current = increment(current)
		i++
	}
	return total
}

// series calc
func double(a int) int {
	return a * 2
}

// combine
func subSquares(a int, b int) int {
	return (int(Sqrt(Abs(float64(a*a - b*b)))))
}

// increment
func addRand(a int) int {
	return a + Intn(10)
}

func main() {
	Println(combineSeries(5, double, subSquares, addRand, 1, 1))
}
