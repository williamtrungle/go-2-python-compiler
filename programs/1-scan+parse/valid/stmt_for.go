package main
func main() {
    for {}
    for ;; {}
    for true {}
    for ;true; {}

    for a := 1 ;; {}
    for a = 1;; {}
    for a++ ;; {}
    for a-- ;; {}

    for ;; a = 1 {}
    for ;; a++ {}
    for ;; a-- {}
    for a++;a + 2; {}
    for ;a(3*"string"%b.c/-0x2<<3)+a[3/1];a++ {}
    for a++;true;a++ {}
}
