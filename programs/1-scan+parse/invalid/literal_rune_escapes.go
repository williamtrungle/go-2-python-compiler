// invalid escaped runes
package main
var x rune
func main() {
    x = '\c'
    x = '\d'
    x = '\e'
    x = '\g'
    x = '\h'
    x = '\i'
    x = '\j'
    x = '\k'
    x = '\l'
    x = '\m'
    x = '\o'
    x = '\p'
    x = '\q'
    x = '\s'
    x = '\u'
    x = '\w'
    x = '\x'
    x = '\y'
    x = '\z'
}
