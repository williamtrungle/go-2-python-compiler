// multi return type and values
package main
func foo(a int) (int, int) {
    b := 1
    return a, b
}
