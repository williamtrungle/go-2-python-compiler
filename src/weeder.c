#include "weeder.h"
#include "error.h"

void assertEXPR_LIST_is_IDENT_LIST(EXPR *e) {
  assertEXPR_is_IDENT(e);
  while (e->next != NULL) {
    assertEXPR_LIST_is_IDENT_LIST(e->next);
  }
}

void assertEXPR_is_IDENT(EXPR* e) {
  if (e->kind != IDENT_LITERAL && e->kind != LIST_EXPR) {
    error_no_line("only identifiers may be used as the left hand side list of a type or variable declaration.");
    //error
  }
}

void assertEXPR_is_not_blankID(EXPR* e) {
  if (e->kind == IDENT_LITERAL) {
    if (strcmp(e->val.stringLiteral, "_") == 0) {
      error_no_line("Blank identifier cannot be used for function name");
      // error
    }
  }
}

void assertIndexing(EXPR* e) {
  if (e->val.binary.lhs->kind != IDENT_LITERAL 
  && e->val.binary.lhs->kind != INDEX_EXPR
  && e->val.binary.lhs->kind != FUNC_EXPR
  && e->val.binary.lhs->kind != MEMBER_EXPR) { //TODO: update with proper enum valu
    error_no_line("left side of indexed expression must be an identifier, function call, member expression, or indexed expression");
    // error
  }
}

void assertIncDec(STMT* s) {
  int kind = s->val.incdecstmt->kind;
  // printf("incdec kind %d\n", kind);
  if (kind != IDENT_LITERAL && kind != INDEX_EXPR && kind != MEMBER_EXPR) {
    // error
    error_no_line("left side of incdec (++/--) statement must be an identifier, member expression, or an indexed expression");
  }
  if (kind == IDENT_LITERAL) {
    assertEXPR_is_not_blankID(s->val.incdecstmt);
  }
}

void assertEXPR_is_func(EXPR* e) {
  // printf("testing for func\n");
  if (e->kind != FUNC_EXPR) {
    // error
    error_no_line("non-function call expression statements are not supported in GoLite.");
  }
  if (strcmp(e->val.func.name, "_") == 0) {
    // error
    error_no_line("cannot call a function with blank identifier");
  }
}

void assert_only_one_default(CASE* ccl, bool found) {
  if (ccl->kind == DEFAULT_CASE) {
    if (found) {
      error_no_line("only one default clause allowed per switch statement.");
    }
    if (ccl->next != NULL) {
      assert_only_one_default(ccl->next, true);
    }
  } else {
    if (ccl->next != NULL) {
      assert_only_one_default(ccl->next, false);
    }
  }
}

void assertSTMT_LIST_has_no_break(STMT* block) {
  STMT* b;
  if (block == NULL || block->val.stmt != NULL) {
    b = block;
  } else {
    return;
  }
  while (b != NULL && b->val.stmt != NULL) {
    if (b->val.stmt->kind == BREAK_STMT) {
      error_no_line("break statements are only allowed inside a for loop or switch statement.");
      // error
    } 
    b = b->next;
  }
}

void assertSTMT_LIST_has_no_cont(STMT* block) {
  STMT* b;
  if (block == NULL || block->val.stmt != NULL) {
    b = block;
  } else {
    return;
  }
  while (b != NULL && b->val.stmt != NULL) {
    if (b->val.stmt->kind == CONTINUE_STMT) {
      error_no_line("continue statements are only allowed inside a for loop.");
      // error
    } 
    b = b->next;
  }
}

int getEXPR_LIST_length(EXPR* s) {
  int l = 0;
  while (s != NULL) {
    l++;
    s = s->next;
  }
  return l;
}

void assertEXPR_LISTS_same_size(EXPR* s1, EXPR* s2) {
  // printf("lists_same_size %d %d\n", getEXPR_LIST_length(s1), getEXPR_LIST_length(s2));
  if (getEXPR_LIST_length(s1) != getEXPR_LIST_length(s2)) {
    error_no_line("lists on both sides of an assignment must be of equal lengths.");
    // error
  }
}

void weederPROG(PROG *p){
  if (strcmp(p->packagename, "_") == 0) {
    error_no_line("package name cannot be a blank identifier");
    // error
  }
  weederDECL_LIST(p->topdcls);
}

void assertNoAssignsInPARA_LIST(PARA *p) {
  while (p != NULL) {
    if (p->kind == VAR_ASSIGN_PARA || p->kind == VAR_TYPE_ASSIGN_PARA) {
      error_no_line("cannot have assignments in structs or functions.");
    }
    p = p->next;
  }
}

void weederTYPE(TYPE *t) {
  if (t == NULL) {
    return;
  }
  switch (t->kind) {
    case IDENT_TYPE:
      if (strcmp(t->val.identtype, "_") == 0) {
        error_no_line("blank identifier cannot be used as a type identifier");
      }
      break;
    case STRUCT_TYPE: // make sure no assigns in struct
      assertNoAssignsInPARA_LIST(t->val.field);
      weederPARA_LIST(t->val.field);
      break;
    case SLICE_TYPE:
      break;
    case ARRAY_TYPE:
      break;
  }
}

void weederEXPR_LIST_allow_blank_id(EXPR* e_) {
  EXPR *e = e_;
  if(e == NULL) {
    return;
  }
  while (e != NULL) {
    weederEXPR(e);
    e = e->next;
  }
}

void weederEXPR_LIST(EXPR* e_) {
  EXPR *e = e_;
  if(e == NULL) {
    return;
  }
  while (e != NULL) {
    if (e->kind == IDENT_LITERAL) {
      assertEXPR_is_not_blankID(e);
    }
    weederEXPR(e);
    e = e->next;
  }
}

void weederEXPR(EXPR *e){
  // printf("EXPR %d\n", e->kind);
  if (e == NULL) {
    return;
  }
  switch(e->kind){
  case DEC_LITERAL:
    break;
  case OCT_LITERAL:
    break;
  case HEX_LITERAL:
    break;
  case RAW_LITERAL:
    break;
  case STRING_LITERAL:
    break;
  case RUNE_LITERAL:
    break;
  case BOOL_LITERAL:
    break;
  case FLOAT_LITERAL:
    break;
  case IDENT_LITERAL:
    break;
  case LIST_EXPR:
    break;
  case FUNC_EXPR:
    assertEXPR_is_func(e);
    weederEXPR_LIST(e->val.func.args);
    break;
  case MEMBER_EXPR:
    assertEXPR_is_not_blankID(e->val.binary.lhs);
    assertEXPR_is_not_blankID(e->val.binary.rhs);
    weederEXPR(e->val.binary.lhs);
    weederEXPR(e->val.binary.rhs); 
    break;
  case EQ_EXPR:
  assertEXPR_is_not_blankID(e->val.binary.lhs);
  assertEXPR_is_not_blankID(e->val.binary.rhs);
    weederEXPR(e->val.binary.lhs);
    weederEXPR(e->val.binary.rhs);
    break; 
  case NE_EXPR:
  assertEXPR_is_not_blankID(e->val.binary.lhs);
  assertEXPR_is_not_blankID(e->val.binary.rhs);
    weederEXPR(e->val.binary.lhs);
    weederEXPR(e->val.binary.rhs);
    break;
  case LT_EXPR:
  assertEXPR_is_not_blankID(e->val.binary.lhs);
  assertEXPR_is_not_blankID(e->val.binary.rhs);
    weederEXPR(e->val.binary.lhs);
    weederEXPR(e->val.binary.rhs);
    break;
  case LE_EXPR:
  assertEXPR_is_not_blankID(e->val.binary.lhs);
  assertEXPR_is_not_blankID(e->val.binary.rhs);
    weederEXPR(e->val.binary.lhs);
    weederEXPR(e->val.binary.rhs);
    break;
  case GT_EXPR:
  assertEXPR_is_not_blankID(e->val.binary.lhs);
  assertEXPR_is_not_blankID(e->val.binary.rhs);
    weederEXPR(e->val.binary.lhs);
    weederEXPR(e->val.binary.rhs);
    break;
  case GE_EXPR:
  assertEXPR_is_not_blankID(e->val.binary.lhs);
  assertEXPR_is_not_blankID(e->val.binary.rhs);
    weederEXPR(e->val.binary.lhs);
    weederEXPR(e->val.binary.rhs);
    break;
  case ADD_EXPR:
  assertEXPR_is_not_blankID(e->val.binary.lhs);
  assertEXPR_is_not_blankID(e->val.binary.rhs);
    weederEXPR(e->val.binary.lhs);
    weederEXPR(e->val.binary.rhs);
    break;
  case MIN_EXPR:
  assertEXPR_is_not_blankID(e->val.binary.lhs);
  assertEXPR_is_not_blankID(e->val.binary.rhs);
    weederEXPR(e->val.binary.lhs);
    weederEXPR(e->val.binary.rhs);
    break;
  case MUL_EXPR:
  assertEXPR_is_not_blankID(e->val.binary.lhs);
  assertEXPR_is_not_blankID(e->val.binary.rhs);
    weederEXPR(e->val.binary.lhs);
    weederEXPR(e->val.binary.rhs);
    break;
  case DIV_EXPR:
  assertEXPR_is_not_blankID(e->val.binary.lhs);
  assertEXPR_is_not_blankID(e->val.binary.rhs);
    weederEXPR(e->val.binary.lhs);
    weederEXPR(e->val.binary.rhs);
    break;
  case MOD_EXPR:
  assertEXPR_is_not_blankID(e->val.binary.lhs);
  assertEXPR_is_not_blankID(e->val.binary.rhs);
    weederEXPR(e->val.binary.lhs);
    weederEXPR(e->val.binary.rhs);
    break;
  case AND_EXPR:
  assertEXPR_is_not_blankID(e->val.binary.lhs);
  assertEXPR_is_not_blankID(e->val.binary.rhs);
    weederEXPR(e->val.binary.lhs);
    weederEXPR(e->val.binary.rhs);
    break;
  case OR_EXPR:
  assertEXPR_is_not_blankID(e->val.binary.lhs);
  assertEXPR_is_not_blankID(e->val.binary.rhs);
    weederEXPR(e->val.binary.lhs);
    weederEXPR(e->val.binary.rhs);
    break;
  case LOR_EXPR:
  assertEXPR_is_not_blankID(e->val.binary.lhs);
  assertEXPR_is_not_blankID(e->val.binary.rhs);
    weederEXPR(e->val.binary.lhs);
    weederEXPR(e->val.binary.rhs);
    break;
  case LAND_EXPR:
  assertEXPR_is_not_blankID(e->val.binary.lhs);
  assertEXPR_is_not_blankID(e->val.binary.rhs);
    weederEXPR(e->val.binary.lhs);
    weederEXPR(e->val.binary.rhs);
    break;
  case XOR_EXPR:
      assertEXPR_is_not_blankID(e->val.binary.lhs);
    assertEXPR_is_not_blankID(e->val.binary.rhs);
    weederEXPR(e->val.binary.lhs);
    weederEXPR(e->val.binary.rhs);
    break;
  case NXOR_EXPR:
      assertEXPR_is_not_blankID(e->val.binary.lhs);
    assertEXPR_is_not_blankID(e->val.binary.rhs);
    weederEXPR(e->val.binary.lhs);
    weederEXPR(e->val.binary.rhs);
    break;
  case LSHIFT_EXPR:
      assertEXPR_is_not_blankID(e->val.binary.lhs);
    assertEXPR_is_not_blankID(e->val.binary.rhs);
    weederEXPR(e->val.binary.lhs);
    weederEXPR(e->val.binary.rhs);
    break;
  case RSHIFT_EXPR:
      assertEXPR_is_not_blankID(e->val.binary.lhs);
    assertEXPR_is_not_blankID(e->val.binary.rhs);
    weederEXPR(e->val.binary.lhs);
    weederEXPR(e->val.binary.rhs);
    break;
  case NOT_EXPR:
    assertEXPR_is_not_blankID(e->val.unary);
    weederEXPR(e->val.unary);
    break;
  case UADD_EXPR:
  assertEXPR_is_not_blankID(e->val.unary);
    weederEXPR(e->val.unary);  
    break;
  case UMIN_EXPR:
  assertEXPR_is_not_blankID(e->val.unary);
    weederEXPR(e->val.unary);  
    break;
  case UXOR_EXPR:
  assertEXPR_is_not_blankID(e->val.unary);
    weederEXPR(e->val.unary);  
    break;
  case INDEX_EXPR:
    assertIndexing(e);
    break;
  }
} 

void weederSTMT(STMT *s){
  // printf("STMT %d\n", s->kind);
  if (s == NULL) {
    return;
  }
  switch(s->kind){
  case LIST_STMT:
    // printf("list stmt arg %d\n", s->val.stmt->kind);
    break;
  case AADD_STMT:
    assertEXPR_LISTS_same_size(s->val.assign.lhs, s->val.assign.rhs);
    weederEXPR_LIST_allow_blank_id(s->val.assign.lhs);
    weederEXPR_LIST(s->val.assign.rhs);
    break;
  case AMIN_STMT:
    assertEXPR_LISTS_same_size(s->val.assign.lhs, s->val.assign.rhs);
    weederEXPR_LIST_allow_blank_id(s->val.assign.lhs);
    weederEXPR_LIST(s->val.assign.rhs);
    break;
  case AMUL_STMT:
    assertEXPR_LISTS_same_size(s->val.assign.lhs, s->val.assign.rhs);
    weederEXPR_LIST_allow_blank_id(s->val.assign.lhs);
    weederEXPR_LIST(s->val.assign.rhs);
    break;
  case ADIV_STMT:
    assertEXPR_LISTS_same_size(s->val.assign.lhs, s->val.assign.rhs);
    weederEXPR_LIST_allow_blank_id(s->val.assign.lhs);
    weederEXPR_LIST(s->val.assign.rhs);
    break;
  case AMOD_STMT:
    assertEXPR_LISTS_same_size(s->val.assign.lhs, s->val.assign.rhs);
    weederEXPR_LIST_allow_blank_id(s->val.assign.lhs);
    weederEXPR_LIST(s->val.assign.rhs);
    break;
  case AOR_STMT:
    assertEXPR_LISTS_same_size(s->val.assign.lhs, s->val.assign.rhs);
    weederEXPR_LIST_allow_blank_id(s->val.assign.lhs);
    weederEXPR_LIST(s->val.assign.rhs);
    break;
  case AXOR_STMT:
    assertEXPR_LISTS_same_size(s->val.assign.lhs, s->val.assign.rhs);
    weederEXPR_LIST_allow_blank_id(s->val.assign.lhs);
    weederEXPR_LIST(s->val.assign.rhs);
    break;
  case AAND_STMT:
    assertEXPR_LISTS_same_size(s->val.assign.lhs, s->val.assign.rhs);
    weederEXPR_LIST_allow_blank_id(s->val.assign.lhs);
    weederEXPR_LIST(s->val.assign.rhs);
    break;
  case ANXOR_STMT:
    assertEXPR_LISTS_same_size(s->val.assign.lhs, s->val.assign.rhs);
    weederEXPR_LIST_allow_blank_id(s->val.assign.lhs);
    weederEXPR_LIST(s->val.assign.rhs);
    break;      
  case ALSHIFT_STMT:
    assertEXPR_LISTS_same_size(s->val.assign.lhs, s->val.assign.rhs);
    weederEXPR_LIST_allow_blank_id(s->val.assign.lhs);
    weederEXPR_LIST(s->val.assign.rhs);
    break;    
  case ARSHIFT_STMT:
    assertEXPR_LISTS_same_size(s->val.assign.lhs, s->val.assign.rhs);
    weederEXPR_LIST_allow_blank_id(s->val.assign.lhs);
    weederEXPR_LIST(s->val.assign.rhs);
    break;    
  case BLOCK_STMT:
    if (s->val.block != NULL && s->val.block->val.stmt != NULL) {
      assertSTMT_LIST_has_no_break(s->val.block->val.stmt);
      assertSTMT_LIST_has_no_cont(s->val.block->val.stmt);
      weederSTMT_LIST(s->val.block->val.stmt);
    }
    break; 
  case ASSIGN_STMT:
    // printf("hello\n");
    assertEXPR_LISTS_same_size(s->val.assign.lhs, s->val.assign.rhs);
    weederEXPR_LIST_allow_blank_id(s->val.assign.lhs);
    weederEXPR_LIST(s->val.assign.rhs);
    break;
  case SHORTDECL_STMT:
    assertEXPR_LISTS_same_size(s->val.assign.lhs, s->val.assign.rhs);
    weederEXPR_LIST_allow_blank_id(s->val.assign.lhs);
    weederEXPR_LIST(s->val.assign.rhs);
    break;
  case VAR_DECL_STMT:
    weederPARA_LIST(s->val.specs); 
    break;
  case TYPE_DECL_STMT:
    weederPARA_LIST(s->val.specs); 
    break;
  case INCR_STMT:
    assertIncDec(s);
    weederEXPR(s->val.incdecstmt);
    break;
  case DECR_STMT:
    assertIncDec(s);
    weederEXPR(s->val.incdecstmt);
    break;
  case FOR_STMT:
    // printf("FOUND A FOR STMT\n");
    if(s->val.forstmt.pre != NULL){
      weederSTMT(s->val.forstmt.pre);
    }
    if(s->val.forstmt.cond != NULL){
      assertEXPR_is_not_blankID(s->val.forstmt.cond);
      weederEXPR(s->val.forstmt.cond);
    }
    if(s->val.forstmt.post != NULL){
      weederSTMT(s->val.forstmt.post);
    }
    weederSTMT_LIST(s->val.forstmt.block->val.stmt);
    break;
  case IF_STMT: //TODO: update this to new spec
    if (s->val.ifstmt.pre != NULL) {
      weederSTMT(s->val.ifstmt.pre);
    }
    if (s->val.ifstmt.cond != NULL) {
      assertEXPR_is_not_blankID(s->val.ifstmt.cond);
      weederEXPR(s->val.ifstmt.cond);
    }
    if (s->val.ifstmt.block != NULL && s->val.ifstmt.block->val.stmt != NULL) {
      assertSTMT_LIST_has_no_break(s->val.ifstmt.block->val.stmt);
      assertSTMT_LIST_has_no_cont(s->val.ifstmt.block->val.stmt);
      weederSTMT_LIST(s->val.ifstmt.block->val.stmt);
    }
    break;
  case IF_ELSE_STMT:
    if (s->val.ifelsestmt.pre != NULL) {
      weederSTMT(s->val.ifelsestmt.pre);
    }
    if (s->val.ifelsestmt.cond != NULL) {
      assertEXPR_is_not_blankID(s->val.ifstmt.cond);
      weederEXPR(s->val.ifelsestmt.cond);
    }
    if (s->val.ifelsestmt.ifblock != NULL && s->val.ifelsestmt.ifblock->val.stmt != NULL) {
      assertSTMT_LIST_has_no_break(s->val.ifelsestmt.ifblock->val.stmt);
      assertSTMT_LIST_has_no_cont(s->val.ifelsestmt.ifblock->val.stmt);
      weederSTMT_LIST(s->val.ifelsestmt.ifblock->val.stmt);
    }
    if (s->val.ifelsestmt.elseblock != NULL && s->val.ifelsestmt.elseblock->val.stmt != NULL) {
      assertSTMT_LIST_has_no_break(s->val.ifelsestmt.elseblock->val.stmt);
      assertSTMT_LIST_has_no_cont(s->val.ifelsestmt.elseblock->val.stmt);
      weederSTMT_LIST(s->val.ifelsestmt.elseblock->val.stmt);
    }
    break;
  case IF_ELIF_STMT:
    if (s->val.ifelifstmt.pre != NULL) {
      weederSTMT(s->val.ifelifstmt.pre);
    }
    if (s->val.ifelifstmt.cond != NULL) {
    assertEXPR_is_not_blankID(s->val.ifstmt.cond);
    weederEXPR(s->val.ifelifstmt.cond);
    }
    if (s->val.ifelifstmt.ifblock != NULL && s->val.ifelifstmt.ifblock->val.stmt != NULL) {
      assertSTMT_LIST_has_no_break(s->val.ifelifstmt.ifblock->val.stmt);
      assertSTMT_LIST_has_no_cont(s->val.ifelifstmt.ifblock->val.stmt);
      weederSTMT_LIST(s->val.ifelifstmt.ifblock->val.stmt);
    }
    if (s->val.ifelifstmt.elifstmt != NULL) {
      weederSTMT(s->val.ifelifstmt.elifstmt);
    }
    break;
  case PRINT_STMT:
    // printf("print stmt\n");
    weederEXPR_LIST(s->val.printarg);
    break;
  case PRINTLN_STMT:
    weederEXPR_LIST(s->val.printarg);
    break;
  case RETURN_STMT:
    weederEXPR(s->val.returnval);
    break; 
  case SWITCH_STMT:
    if (s->val.switchstmt.pre != NULL) {
        weederSTMT(s->val.switchstmt.pre);  
    }
    if (s->val.switchstmt.test != NULL) {
      assertEXPR_is_not_blankID(s->val.switchstmt.test);
      weederEXPR(s->val.switchstmt.test);
    } 
    if (s->val.switchstmt.clause != NULL) {
      assert_only_one_default(s->val.switchstmt.clause, false);
      weederCASE_LIST(s->val.switchstmt.clause);
    }
    break; 
  case BREAK_STMT:
    break;
  case CONTINUE_STMT:
    break;
  case FUNC_STMT:
    if(s->val.func != NULL) {
      assertEXPR_is_func(s->val.func);
      weederEXPR_LIST(s->val.func);
    }
    break;
  }
} 

void weederSTMT_LIST(STMT *s_){
  if(s_ != NULL){
  STMT *s = s_;
  // printf("STMT_LIST %d\n", s_->kind);
    weederSTMT(s->val.stmt);
    if(s->next != NULL){
      weederSTMT_LIST(s->next);
    }
  }
}

void weederDECL(DECL *d){
  switch(d->kind) {
  case FUNC_DECL:
    if (d->specs != NULL) {
      if (d->specs->funcpara.block != NULL && d->specs->funcpara.block->val.stmt != NULL) {
      assertSTMT_LIST_has_no_break(d->specs->funcpara.block->val.stmt);
      assertSTMT_LIST_has_no_cont(d->specs->funcpara.block->val.stmt);
      weederSTMT_LIST(d->specs->funcpara.block->val.stmt);
      }
    assertNoAssignsInPARA_LIST(d->specs);
    weederPARA_LIST(d->specs);
    }
    break;
  default:
    if (d->specs != NULL) {
      weederPARA_LIST(d->specs);
    }
    break;
  }
}

void weederDECL_LIST(DECL *d_){
  DECL *d = d_;
  // printf("DECL_LIST\n");
  if (d != NULL) {
    weederDECL(d);
    if (d->next != NULL) {
      weederDECL_LIST(d->next);
    }
  }
}

void weederPARA(PARA *p){
  // printf("PARA\n");
  if (p == NULL) {
    return;
  }
  switch(p->kind){
  case VAR_TYPE_PARA:
    weederEXPR_LIST_allow_blank_id(p->expr);
    weederTYPE(p->type);
    break;
  case VAR_ASSIGN_PARA:
    assertEXPR_LISTS_same_size(p->expr, p->rhs);
    weederEXPR_LIST_allow_blank_id(p->expr);
    weederEXPR_LIST(p->rhs);
    break;
  case VAR_TYPE_ASSIGN_PARA:
    weederEXPR_LIST_allow_blank_id(p->expr);
    assertEXPR_LISTS_same_size(p->expr, p->rhs);
    weederEXPR_LIST(p->rhs);
    weederTYPE(p->type);
    break;
  case FUNC_PARA:
    weederEXPR_LIST_allow_blank_id(p->expr);
    weederEXPR_LIST(p->rhs);
    weederTYPE(p->type);
    break;
  }
} 

void weederPARA_LIST(PARA *p_){
  PARA *p = p_;
  if(p != NULL){
    weederPARA(p);
    if(p->next != NULL) {
      weederPARA_LIST(p->next);
    }
  }
}

void weederCASE(CASE *c){
  if (c == NULL) {
    return;
  }
  switch(c->kind){
  case CLAUSE_CASE:
    if (c->cond != NULL) {
      weederEXPR_LIST(c->cond);
    }
    if (c-> body != NULL) {
      assertSTMT_LIST_has_no_cont(c->body);
      weederSTMT_LIST(c->body);
    }
    break;
  case DEFAULT_CASE:
    if (c-> body != NULL) {
      assertSTMT_LIST_has_no_cont(c->body);
      weederSTMT_LIST(c->body);
    }
    break;
  }
}

void weederCASE_LIST(CASE *c_){
  CASE *c = c_;
  if(c != NULL){
    weederCASE(c);
    if(c->next != NULL) {
      weederCASE_LIST(c->next);
    }
  }
}