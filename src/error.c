#include <stdio.h>
#include <stdlib.h>
#include "error.h"

// Usage:
// return error(lineno, identifier);
int error(int line, char *msg)
{
    fprintf(stderr, "Error: (line %d) %s\n", line, msg);
    if (yyin != NULL) { fclose(yyin); }
    exit(1);
}

int error_no_line(char *msg)
{
    fprintf(stderr, "Error: %s\n", msg);
    if (yyin != NULL) { fclose(yyin); }
    exit(1);
}