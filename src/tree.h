#ifndef TREE_H
#define TREE_H
#include <stdbool.h>

typedef struct PROG PROG;
typedef struct DECL DECL;
typedef struct PARA PARA;
typedef struct TYPE TYPE;
typedef struct EXPR EXPR;
typedef struct STMT STMT;
typedef struct CASE CASE;
typedef enum DeclKind DeclKind;
typedef enum ParaKind ParaKind;
typedef enum TypeKind TypeKind;
typedef enum ExprKind ExprKind;
typedef enum StmtKind StmtKind;
typedef enum CaseKind CaseKind;

// ----------------------------------------------------------------------------
// Struct Implementations

struct PROG {
	char *packagename;
	DECL *topdcls;
};

enum DeclKind {
    VAR_DECL,
    TYPE_DECL,
	FUNC_DECL
};

struct DECL {
    DECL *next;
	DeclKind kind;
    PARA *specs; 
};

enum ParaKind {
    VAR_TYPE_PARA,
    VAR_ASSIGN_PARA,
    VAR_TYPE_ASSIGN_PARA,
    FUNC_PARA
};

struct PARA {
    ParaKind kind;
    PARA *next;
    EXPR *expr; // lhs identifiers or function name
    TYPE *type;
    EXPR *rhs; // assign exp or function arguments
    struct { PARA *args; STMT *block; } funcpara;// function body
};

enum TypeKind {
    IDENT_TYPE,
    STRUCT_TYPE,
    SLICE_TYPE,
    ARRAY_TYPE
};

struct TYPE {
    TypeKind kind;
    union {
        char *identtype; // ident type
        PARA *field; // struct type
        struct { int size; TYPE *next; } array;
        struct { TYPE *next; } slice;
    } val;
};

enum ExprKind {
    LIST_EXPR,
	DEC_LITERAL,
	OCT_LITERAL,
	HEX_LITERAL,
	FLOAT_LITERAL,
	RAW_LITERAL,
	STRING_LITERAL,
	RUNE_LITERAL,
	BOOL_LITERAL,
	IDENT_LITERAL,
	MEMBER_EXPR,
    INDEX_EXPR,
	EQ_EXPR,
	NE_EXPR,
	LT_EXPR,
	LE_EXPR,
	GT_EXPR,
	GE_EXPR,
	ADD_EXPR,
	MIN_EXPR,
	MUL_EXPR,
	DIV_EXPR,
	MOD_EXPR,
	AND_EXPR,
	OR_EXPR,
	LOR_EXPR,
	LAND_EXPR,
	XOR_EXPR,
	NXOR_EXPR,
	LSHIFT_EXPR,
	RSHIFT_EXPR,
	NOT_EXPR,
	UADD_EXPR,
	UMIN_EXPR,
	UXOR_EXPR,
	FUNC_EXPR
};

struct EXPR {
	ExprKind kind;
    EXPR *next;
	union {
		int intLiteral;
		double floatLiteral;
		bool boolLiteral;
		char *stringLiteral;
		struct {char *name; EXPR *args;} func;
		struct { EXPR *lhs; EXPR *rhs; } binary;
		EXPR *unary;
		struct { EXPR *lhs; char *rhs; } member;
	} val;
};

enum StmtKind {
    LIST_STMT,
	BLOCK_STMT,
    ASSIGN_STMT,
    AADD_STMT,
    AMIN_STMT,
    AMUL_STMT,
    ADIV_STMT,
    AMOD_STMT,
    AOR_STMT,
    AAND_STMT,
    AXOR_STMT,
    ANXOR_STMT,
    ALSHIFT_STMT,
    ARSHIFT_STMT,
	SHORTDECL_STMT,
	VAR_DECL_STMT,
	TYPE_DECL_STMT,
	INCR_STMT,
	DECR_STMT,
	FOR_STMT,
	IF_STMT,
	IF_ELIF_STMT,
	IF_ELSE_STMT,
	PRINT_STMT,
	PRINTLN_STMT, 
	RETURN_STMT,
	SWITCH_STMT,
	BREAK_STMT,
	CONTINUE_STMT,
	FUNC_STMT
};

struct STMT {
    STMT *next;
	StmtKind kind;
	union {
        STMT *stmt;
		PARA *specs;
		EXPR *printarg;
		EXPR *returnval;
        struct { STMT *pre; EXPR *cond; STMT *block; } ifstmt;
        struct { STMT *pre; EXPR *cond; STMT *ifblock; STMT *elifstmt; } ifelifstmt;
        struct { STMT *pre; EXPR *cond; STMT *ifblock; STMT *elseblock; } ifelsestmt;
		STMT *block;
		EXPR *func;
		EXPR *incdecstmt;
		struct { STMT *pre; EXPR *cond; STMT *post; STMT *block;} forstmt;
		struct { STMT *pre; EXPR *test; CASE *clause;} switchstmt;
		struct { EXPR *lhs; EXPR *rhs;} assign;
	} val;
};

enum CaseKind {
	CLAUSE_CASE,
	DEFAULT_CASE
};

struct CASE {
    CASE *next;
	CaseKind kind;
	EXPR *cond;
	STMT *body;
};

// ----------------------------------------------------------------------------
// Prototypes

PROG *makePROG(char *packagename, DECL *topdcls);

DECL *makeDECL_vdcl(DECL *next, PARA *specs);
DECL *makeDECL_tdcl(DECL *next, PARA *specs);
DECL *makeDECL_func(DECL *next, PARA *specs);

PARA *makePARA(PARA *next, ParaKind kind, EXPR *expr, TYPE *type);
PARA *makePARA_assign(PARA *next, ParaKind kind, EXPR *expr, TYPE *type, EXPR *rhs);
PARA *makePARA_func(EXPR *name, PARA *args, TYPE *returntype, STMT *block);

TYPE *makeTYPE_ident(char *identtype);
TYPE *makeTYPE_struct(PARA *field);
TYPE *makeTYPE_slice(TYPE *next);
TYPE *makeTYPE_array(int size, TYPE *next);

EXPR *makeEXPR_BinaryEXPR(EXPR *next, ExprKind kind, EXPR *lhs, EXPR *rhs);
EXPR *makeEXPR_BinaryEXPR_L(EXPR *next, ExprKind kind, EXPR *lhs, EXPR *rhs);
EXPR *makeEXPR_UnaryEXPR(EXPR *next, ExprKind kind, EXPR *unary);
EXPR *makeEXPR_UnaryEXPR_L(EXPR *next, ExprKind kind, EXPR *unary);

EXPR *makeEXPR_identifier(EXPR *next, char *identifier);
EXPR *makeEXPR_floatLiteral(EXPR *next, float f);
EXPR *makeEXPR_stringLiteral(EXPR *next, char *s);
EXPR *makeEXPR_rawLiteral(EXPR *next, char *s);
EXPR *makeEXPR_runeLiteral(EXPR *next, char *s); 
EXPR *makeEXPR_decLiteral(EXPR *next, int i);
EXPR *makeEXPR_octLiteral(EXPR *next, int i);
EXPR *makeEXPR_hexLiteral(EXPR *next, int i);
EXPR *makeEXPR_boolLiteral(EXPR *next, bool b);
EXPR *makeEXPR_func(EXPR *next, char *name, EXPR *args);
EXPR *makeEXPR_member(EXPR *next, EXPR* lhs, char* rhs);

STMT *makeSTMT_LIST(STMT *next, STMT *stmt);
STMT *makeSTMT_BLOCK(STMT* block);
STMT *makeSTMT_ASSIGN(StmtKind kind, EXPR *lhs, EXPR *rhs);
STMT *makeSTMT_VDECL(PARA *specs);
STMT *makeSTMT_TDECL(PARA *specs);
STMT *makeSTMT_INC(EXPR *incdecstmt);
STMT *makeSTMT_DEC(EXPR *incdecstmt);
STMT *makeSTMT_FOR(STMT *pre, EXPR *cond, STMT *post, STMT *block);
STMT *makeSTMT_IF(    STMT *pre, EXPR *cond, STMT *block);
STMT *makeSTMT_IFELSE(STMT *pre, EXPR *cond, STMT *ifblock, STMT *elseblock);
STMT *makeSTMT_IFELIF(STMT *pre, EXPR *cond, STMT *ifblock, STMT *elifstmt); 
STMT *makeSTMT_PRINT(EXPR *printarg);
STMT *makeSTMT_PRINTLN(EXPR *printarg); 
STMT *makeSTMT_RETURN(EXPR* returnval);
STMT *makeSTMT_SWITCH(STMT *pre, EXPR *test, CASE *clause);
STMT *makeSTMT_CONT();
STMT *makeSTMT_BREAK();
STMT *makeSTMT_FUNC(EXPR* func); // exp must be func

CASE *makeCASE(CASE *next, CaseKind kind, EXPR *cond, STMT *body);
#endif
