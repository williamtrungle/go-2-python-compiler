#include <stdio.h>
#include <string.h>
#include "tree.h"

void assertEXPR_LIST_is_IDENT_LIST(EXPR *e);
void assertEXPR_is_IDENT(EXPR* e);
void assertstring_is_not_blankID(EXPR* e);
void assertIndexing(EXPR* e);
void assertIncDec(STMT* s);
void assertEXPR_is_func(EXPR* e);
void assert_only_one_default(CASE* ccl, bool found);
void assertSTMT_LIST_has_no_break(STMT* block);
void assertSTMT_LIST_has_no_cont(STMT* block);
int getEXPR_LIST_length(EXPR* s);
void assertEXPR_LISTS_same_size(EXPR* s1, EXPR* s2);
void weederPROG(PROG *p);
void assertNoAssignsInPARA_LIST(PARA *p);
void weederTYPE(TYPE *t);
void weederEXPR(EXPR *e);
void weederSTMT(STMT *s);
void weederSTMT_LIST(STMT *s);
void weederDECL(DECL *d);
void weederDECL_LIST(DECL *d);
void weederPARA(PARA *p);
void weederPARA_LIST(PARA *p);
void weederCASE(CASE *c);
void weederCASE_LIST(CASE *c);
