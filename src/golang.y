%{
#include <stdio.h>
#include <stdbool.h>
#include "tree.h"

#define YYDEBUG 1

extern PROG *root;
extern FILE *yyin;
extern int yylineno;
int yylex();
void yyerror(const char *s){
    fprintf(stderr, "Error: (line %d) %s\n", yylineno, s);
    fclose(yyin);
    exit(1);
}
%}

%code requires
{
    #include <stdbool.h>
    #include "tree.h"
}
%define parse.error verbose
%locations

%union{
  char *typeval;
  bool boolval;
  float floatval;
  long int intval;
  char *stringval;
  char *id;
  DECL *decls;
  PARA *paras;
  TYPE *types;
  EXPR *exprs;
  STMT *stmts;
  CASE *cases;
 }

%token <boolval> tBOOL
%token <floatval> tFLOAT
%token <intval> tDEC
%token <intval> tOCT
%token <intval> tHEX
%token <stringval> tRAWSTRING
%token <stringval> tSTRING
%token <stringval> tRUNE
%token <id> tIDENTIFIER

%token tBREAK tCASE tCHAN tCONST tCONT tDEFAULT tDEFER tELSE tFT tFOR tFUNC tGO tVAR tPRINT tPRINTLN
%token tGOTO tIF tIMPORT tINTF tMAP tPKG tRANGE tRET tSEL tSTRUCT tSWITCH tTYPE tAPPEND

%token tEQ tNE tLT tLE tGT tGE
%token tADD tMIN tMUL tDIV tMOD tOR tAND
%token tASGN tSHRTASGN tMEMBER
%token tAADD tAMIN tAMUL tADIV tAMOD
%token tAOR tAAND tAXOR tANXOR tALSHIFT tARSHIFT
%token tLOR tLAND tXOR tNXOR tLSHIFT tRSHIFT
%token tCHANOP tNOT tINCR tDECR
%token tLPAREN tRPAREN tLBRACE tRBRACE tLBRACK tRBRACK
%token tELLIPSIS tCOLON tSEMIC tCOMMA

%type <decls> dcls 
%type <types> type
%type <exprs> exp explist identlist functioncall primaryexp unaryexp sequentialexp
%type <paras> vardcl typedcl typespeclist field varspeclist parameterlist funcdcl
%type <stmts> stmt print return for forpost forinit simplestmt if assign_stmt stmts block incdec switch
%type <cases> cases

%left tOR
%left tAND
%left tEQ tNE tLT tLE tGT tGE
%left tADD tMIN tLOR tXOR
%left tMUL tDIV tMOD tLSHIFT tRSHIFT tLAND tNXOR
%left tUMIN tUADD tNOT tUXOR

%%

program: tPKG tIDENTIFIER tSEMIC dcls { root = makePROG($2, $4); }
;

dcls:
  %empty { $$ = NULL; }
| vardcl dcls { $$ = makeDECL_vdcl($2, $1); }
| typedcl dcls { $$ = makeDECL_tdcl($2, $1); }
| funcdcl dcls { $$ = makeDECL_func($2, $1); }
;

// ----------------------------------------------------------------------------
// Type Declarations

typedcl:
  tTYPE tIDENTIFIER type tSEMIC { $$ = makePARA(NULL, VAR_TYPE_PARA, makeEXPR_identifier(NULL, $2), $3); }
| tTYPE tLPAREN typespeclist tRPAREN tSEMIC { $$ = $3; }

typespeclist:
  tIDENTIFIER type tSEMIC { $$ = makePARA(NULL, VAR_TYPE_PARA, makeEXPR_identifier(NULL, $1), $2); }
| tIDENTIFIER type tSEMIC typespeclist { $$ = makePARA($4, VAR_TYPE_PARA, makeEXPR_identifier(NULL, $1), $2); }
;

type:
  tIDENTIFIER { $$ = makeTYPE_ident($1); }
| tLPAREN type tRPAREN { $$ = $2; }
| tSTRUCT tLBRACE field tRBRACE { $$ = makeTYPE_struct($3); }
| tLBRACK tRBRACK type { $$ = makeTYPE_slice($3); }
| tLBRACK tDEC tRBRACK type { $$ = makeTYPE_array($2, $4); }
| tLBRACK tOCT tRBRACK type { $$ = makeTYPE_array($2, $4); }
| tLBRACK tHEX tRBRACK type { $$ = makeTYPE_array($2, $4); }
;

field:
  identlist type tSEMIC { $$ = makePARA(NULL, VAR_TYPE_PARA, $1, $2); }
| identlist type tSEMIC field { $$ = makePARA($4, VAR_TYPE_PARA, $1, $2); }
;

// explist
identlist: 
  tIDENTIFIER { $$ = makeEXPR_identifier(NULL, $1); }
| tIDENTIFIER tCOMMA identlist { $$ = makeEXPR_identifier($3, $1); }
;

// ----------------------------------------------------------------------------
// Variable Declarations


// make vardcl
vardcl:
  tVAR identlist type tSEMIC { $$ = makePARA(NULL, VAR_TYPE_PARA, $2, $3); }
| tVAR identlist tASGN explist tSEMIC { $$ = makePARA_assign(NULL, VAR_ASSIGN_PARA, $2, NULL, $4); }
| tVAR identlist type tASGN explist tSEMIC { $$ = makePARA_assign(NULL, VAR_TYPE_ASSIGN_PARA, $2, $3, $5); }
| tVAR tLPAREN varspeclist tRPAREN tSEMIC { $$ = $3; }
;

varspeclist:
  identlist type tSEMIC { $$ = makePARA(NULL, VAR_TYPE_PARA, $1, $2); }
| identlist tASGN explist tSEMIC { $$ = makePARA_assign(NULL, VAR_ASSIGN_PARA, $1, NULL, $3); }
| identlist type tASGN explist tSEMIC { $$ = makePARA_assign(NULL, VAR_TYPE_ASSIGN_PARA, $1, $2, $4); }
| identlist type tSEMIC varspeclist { $$ = makePARA($4, VAR_TYPE_PARA, $1, $2); }
| identlist tASGN explist tSEMIC varspeclist { $$ = makePARA_assign($5, VAR_ASSIGN_PARA, $1, NULL, $3); }
| identlist type tASGN explist tSEMIC varspeclist { $$ = makePARA_assign($6, VAR_TYPE_ASSIGN_PARA, $1, $2, $4); }
;

functioncall:
  tIDENTIFIER tLPAREN tRPAREN { $$ = makeEXPR_func(NULL, $1, NULL); }
| tIDENTIFIER tLPAREN explist tRPAREN { $$ = makeEXPR_func(NULL, $1, $3); }
;

primaryexp:
  tIDENTIFIER { $$ = makeEXPR_identifier(NULL, $1); }
| tFLOAT { $$ = makeEXPR_floatLiteral(NULL, $1); }
| tDEC { $$ = makeEXPR_decLiteral(NULL, $1); }
| tOCT { $$ = makeEXPR_octLiteral(NULL, $1); }
| tHEX { $$ = makeEXPR_decLiteral(NULL, $1); }
| tSTRING { $$ = makeEXPR_stringLiteral(NULL, $1); }
| tRAWSTRING { $$ = makeEXPR_rawLiteral(NULL, $1); }
| tRUNE { $$ = makeEXPR_runeLiteral(NULL, $1); }
| tBOOL { $$ = makeEXPR_boolLiteral(NULL, $1); }
| functioncall { $$ = $1; }
| tLPAREN exp tRPAREN { $$ = $2; }
;

unaryexp:
  primaryexp { $$ = $1; }
| tADD unaryexp %prec tUADD { $$ = makeEXPR_UnaryEXPR(NULL, UADD_EXPR, $2); }
| tMIN unaryexp %prec tUMIN { $$ = makeEXPR_UnaryEXPR(NULL, UMIN_EXPR, $2); }
| tXOR unaryexp %prec tUXOR { $$ = makeEXPR_UnaryEXPR(NULL, UXOR_EXPR, $2); }
| tNOT unaryexp { $$ = makeEXPR_UnaryEXPR(NULL, NOT_EXPR, $2); }
;

exp:
  unaryexp { $$ = $1; }
| exp tOR exp { $$ = makeEXPR_BinaryEXPR(NULL, OR_EXPR, $1, $3); }
| exp tAND exp { $$ = makeEXPR_BinaryEXPR(NULL, AND_EXPR, $1, $3); }
| exp tEQ exp { $$ = makeEXPR_BinaryEXPR(NULL, EQ_EXPR, $1, $3); }
| exp tNE exp { $$ = makeEXPR_BinaryEXPR(NULL, NE_EXPR, $1, $3); }
| exp tLT exp { $$ = makeEXPR_BinaryEXPR(NULL, LT_EXPR, $1, $3); }
| exp tLE exp { $$ = makeEXPR_BinaryEXPR(NULL, LE_EXPR, $1, $3); }
| exp tGT exp { $$ = makeEXPR_BinaryEXPR(NULL, GT_EXPR, $1, $3); }
| exp tGE exp { $$ = makeEXPR_BinaryEXPR(NULL, GE_EXPR, $1, $3); }
| exp tADD exp { $$ = makeEXPR_BinaryEXPR(NULL, ADD_EXPR, $1, $3); }
| exp tMIN exp { $$ = makeEXPR_BinaryEXPR(NULL, MIN_EXPR, $1, $3); }
| exp tLOR exp { $$ = makeEXPR_BinaryEXPR(NULL, LOR_EXPR, $1, $3); }
| exp tXOR exp { $$ = makeEXPR_BinaryEXPR(NULL, XOR_EXPR, $1, $3); }
| exp tMUL exp { $$ = makeEXPR_BinaryEXPR(NULL, MUL_EXPR, $1, $3); }
| exp tDIV exp { $$ = makeEXPR_BinaryEXPR(NULL, DIV_EXPR, $1, $3); }
| exp tMOD exp { $$ = makeEXPR_BinaryEXPR(NULL, MOD_EXPR, $1, $3); }
| exp tLAND exp { $$ = makeEXPR_BinaryEXPR(NULL, LAND_EXPR, $1, $3); }
| exp tNXOR exp { $$ = makeEXPR_BinaryEXPR(NULL, NXOR_EXPR, $1, $3); }
| exp tLSHIFT exp { $$ = makeEXPR_BinaryEXPR(NULL, LSHIFT_EXPR, $1, $3); }
| exp tRSHIFT exp { $$ = makeEXPR_BinaryEXPR(NULL, RSHIFT_EXPR, $1, $3); }
| sequentialexp { $$ = $1; }
;

explist:
  exp { $$ = $1; }
| unaryexp tCOMMA explist { $$ = makeEXPR_UnaryEXPR($3, LIST_EXPR, $1); }
| exp tOR exp tCOMMA explist { $$ = makeEXPR_BinaryEXPR($5, OR_EXPR, $1, $3); }
| exp tAND exp tCOMMA explist { $$ = makeEXPR_BinaryEXPR($5, AND_EXPR, $1, $3); }
| exp tEQ exp tCOMMA explist { $$ = makeEXPR_BinaryEXPR($5, EQ_EXPR, $1, $3); }
| exp tNE exp tCOMMA explist { $$ = makeEXPR_BinaryEXPR($5, NE_EXPR, $1, $3); }
| exp tLT exp tCOMMA explist { $$ = makeEXPR_BinaryEXPR($5, LT_EXPR, $1, $3); }
| exp tLE exp tCOMMA explist { $$ = makeEXPR_BinaryEXPR($5, LE_EXPR, $1, $3); }
| exp tGT exp tCOMMA explist { $$ = makeEXPR_BinaryEXPR($5, GT_EXPR, $1, $3); }
| exp tGE exp tCOMMA explist { $$ = makeEXPR_BinaryEXPR($5, GE_EXPR, $1, $3); }
| exp tADD exp tCOMMA explist { $$ = makeEXPR_BinaryEXPR($5, ADD_EXPR, $1, $3); }
| exp tMIN exp tCOMMA explist { $$ = makeEXPR_BinaryEXPR($5, MIN_EXPR, $1, $3); }
| exp tLOR exp tCOMMA explist { $$ = makeEXPR_BinaryEXPR($5, LOR_EXPR, $1, $3); }
| exp tXOR exp tCOMMA explist { $$ = makeEXPR_BinaryEXPR($5, XOR_EXPR, $1, $3); }
| exp tMUL exp tCOMMA explist { $$ = makeEXPR_BinaryEXPR($5, MUL_EXPR, $1, $3); }
| exp tDIV exp tCOMMA explist { $$ = makeEXPR_BinaryEXPR($5, DIV_EXPR, $1, $3); }
| exp tMOD exp tCOMMA explist { $$ = makeEXPR_BinaryEXPR($5, MOD_EXPR, $1, $3); }
| exp tLAND exp tCOMMA explist { $$ = makeEXPR_BinaryEXPR($5, LAND_EXPR, $1, $3); }
| exp tNXOR exp tCOMMA explist { $$ = makeEXPR_BinaryEXPR($5, NXOR_EXPR, $1, $3); }
| exp tLSHIFT exp tCOMMA explist { $$ = makeEXPR_BinaryEXPR($5, LSHIFT_EXPR, $1, $3); }
| exp tRSHIFT exp tCOMMA explist { $$ = makeEXPR_BinaryEXPR($5, RSHIFT_EXPR, $1, $3); }
| sequentialexp tCOMMA explist { $$ = makeEXPR_UnaryEXPR($3, LIST_EXPR, $1); }
;

sequentialexp:
  primaryexp tLBRACK exp tRBRACK { $$ = makeEXPR_BinaryEXPR(NULL, INDEX_EXPR, $1, $3); }
| primaryexp tMEMBER tIDENTIFIER { $$ = makeEXPR_member(NULL, $1, $3); }
| sequentialexp tLBRACK exp tRBRACK { $$ = makeEXPR_BinaryEXPR(NULL, INDEX_EXPR, $1, $3); }
| sequentialexp tMEMBER tIDENTIFIER  { $$ = makeEXPR_member(NULL, $1, $3); }
;

// ----------------------------------------------------------------------------
// Function Declarations

funcdcl:
  tFUNC tIDENTIFIER tLPAREN parameterlist tRPAREN block tSEMIC { $$ = makePARA_func(makeEXPR_identifier(NULL, $2), $4, NULL, $6); }
| tFUNC tIDENTIFIER tLPAREN parameterlist tRPAREN type block tSEMIC { $$ = makePARA_func(makeEXPR_identifier(NULL, $2), $4, $6, $7); }
| tFUNC tIDENTIFIER tLPAREN tRPAREN block tSEMIC { $$ = makePARA_func(makeEXPR_identifier(NULL, $2), NULL, NULL, $5); }
| tFUNC tIDENTIFIER tLPAREN tRPAREN type block tSEMIC { $$ = makePARA_func(makeEXPR_identifier(NULL, $2), NULL, $5, $6); }
;
parameterlist:
  identlist type { $$ = makePARA(NULL, VAR_TYPE_PARA, $1, $2); }
| identlist type tCOMMA parameterlist { $$ = makePARA($4, VAR_TYPE_PARA, $1, $2); }
;

block:
  tLBRACE stmts tRBRACE { $$ = makeSTMT_BLOCK($2); }
;

// ----------------------------------------------------------------------------
// Statements

stmts:
  %empty { $$ = NULL; }
| stmt stmts { $$ = makeSTMT_LIST($2, $1); }
;

stmt:
  if { $$ = $1;}
| typedcl { $$ = makeSTMT_TDECL($1); }
| vardcl { $$ = makeSTMT_VDECL($1); }
| simplestmt tSEMIC { $$ = $1;}
| print tSEMIC { $$ = $1;}
| return tSEMIC { $$ = $1;}
| switch tSEMIC { $$ = $1;}
| tBREAK tSEMIC { $$ = makeSTMT_BREAK(); }
| tCONT tSEMIC { $$ = makeSTMT_CONT(); }
| for tSEMIC { $$ = $1;}
| block tSEMIC { $$ = makeSTMT_BLOCK($1); }
;

// 2nd rule is expression-statements
simplestmt:
  %empty { $$ = NULL; }
| exp { $$ = makeSTMT_FUNC($1); }
| incdec { $$ = $1; }
| assign_stmt { $$ = $1;}
| explist tSHRTASGN explist { $$ = makeSTMT_ASSIGN(SHORTDECL_STMT, $1, $3); }
;

assign_stmt:
  explist tASGN explist { $$ = makeSTMT_ASSIGN(ASSIGN_STMT, $1, $3); }
| explist tAADD explist { $$ = makeSTMT_ASSIGN(AADD_STMT, $1, $3); }
| explist tAMIN explist { $$ = makeSTMT_ASSIGN(AMIN_STMT, $1, $3); }
| explist tAMUL explist { $$ = makeSTMT_ASSIGN(AMUL_STMT, $1, $3); }
| explist tADIV explist { $$ = makeSTMT_ASSIGN(ADIV_STMT, $1, $3); }
| explist tAMOD explist { $$ = makeSTMT_ASSIGN(AMOD_STMT, $1, $3); }
| explist tAOR explist { $$ = makeSTMT_ASSIGN(AOR_STMT, $1, $3); }
| explist tAAND explist { $$ = makeSTMT_ASSIGN(AAND_STMT, $1, $3); }
| explist tAXOR explist { $$ = makeSTMT_ASSIGN(AXOR_STMT, $1, $3); }
| explist tANXOR explist { $$ = makeSTMT_ASSIGN(ANXOR_STMT, $1, $3); }
| explist tALSHIFT explist { $$ = makeSTMT_ASSIGN(ALSHIFT_STMT, $1, $3); }
| explist tARSHIFT explist { $$ = makeSTMT_ASSIGN(ARSHIFT_STMT, $1, $3); }
;

incdec:
  exp tINCR { $$ = makeSTMT_INC($1); }
| exp tDECR { $$ = makeSTMT_DEC($1); }
;

for:
  tFOR block { $$ = makeSTMT_FOR(NULL, NULL, NULL, $2); }
| tFOR exp block {$$ = makeSTMT_FOR(NULL, $2, NULL, $3); }
| tFOR forinit tSEMIC tSEMIC forpost block { $$ = makeSTMT_FOR($2, NULL, $5, $6); }
| tFOR forinit tSEMIC exp tSEMIC forpost block { $$ = makeSTMT_FOR($2, $4, $6, $7); }
;

forinit:
  %empty { $$ = NULL; }
| incdec { $$ = $1; }
| assign_stmt { $$ = $1;}
| explist tSHRTASGN explist { $$ = makeSTMT_ASSIGN(SHORTDECL_STMT, $1, $3); }
;

forpost:
  %empty { $$ = NULL; }
| incdec { $$ = NULL; }
| assign_stmt { $$ = $1;}
;

print:
  tPRINT tLPAREN tRPAREN { $$ = makeSTMT_PRINT(NULL); }
| tPRINT tLPAREN explist tRPAREN { $$ = makeSTMT_PRINT($3); }
| tPRINTLN tLPAREN tRPAREN { $$ = makeSTMT_PRINTLN(NULL); }
| tPRINTLN tLPAREN explist tRPAREN { $$ = makeSTMT_PRINTLN($3); }
;

return:
  tRET exp { $$ = makeSTMT_RETURN($2); }
| tRET { $$ = makeSTMT_RETURN(NULL); }
;

if:
  tIF exp block tSEMIC { $$ = makeSTMT_IF(NULL, $2, $3); }
| tIF exp block tELSE if { $$ = makeSTMT_IFELIF(NULL, $2, $3, $5); }
| tIF exp block tELSE block tSEMIC { $$ = makeSTMT_IFELSE(NULL, $2, $3, $5); }
| tIF simplestmt tSEMIC exp block tSEMIC { $$ = makeSTMT_IF($2, $4, $5); }
| tIF simplestmt tSEMIC exp block tELSE if { $$ = makeSTMT_IFELIF($2, $4, $5, $7); }
| tIF simplestmt tSEMIC exp block tELSE block tSEMIC { $$ = makeSTMT_IFELSE($2, $4, $5, $7); }
;

switch:
  tSWITCH tLBRACE tRBRACE { $$ = makeSTMT_SWITCH(NULL, NULL, NULL); }
| tSWITCH exp tLBRACE tRBRACE { $$ = makeSTMT_SWITCH(NULL, $2, NULL); }
| tSWITCH tLBRACE cases tRBRACE { $$ = makeSTMT_SWITCH(NULL, NULL, $3); }
| tSWITCH exp tLBRACE cases tRBRACE { $$ = makeSTMT_SWITCH(NULL, $2, $4); }
| tSWITCH simplestmt tSEMIC tLBRACE tRBRACE { $$ = makeSTMT_SWITCH($2, NULL, NULL); }
| tSWITCH simplestmt tSEMIC exp tLBRACE tRBRACE { $$ = makeSTMT_SWITCH($2, $4, NULL); }
| tSWITCH simplestmt tSEMIC tLBRACE cases tRBRACE { $$ = makeSTMT_SWITCH($2, NULL, $5); }
| tSWITCH simplestmt tSEMIC exp tLBRACE cases tRBRACE { $$ = makeSTMT_SWITCH($2, $4, $6); }
;

cases:
  tCASE explist tCOLON stmts { $$ = makeCASE(NULL, CLAUSE_CASE, $2, $4); }
| tCASE explist tCOLON stmts cases { $$ = makeCASE($5, CLAUSE_CASE, $2, $4); }
| tDEFAULT tCOLON stmts { $$ = makeCASE(NULL, DEFAULT_CASE, NULL, $3); }
| tDEFAULT tCOLON stmts cases { $$ = makeCASE($4, DEFAULT_CASE, NULL, $3); }
;
