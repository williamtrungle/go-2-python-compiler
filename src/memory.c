#include <stdio.h>
#include <stdlib.h>
#include "memory.h"
extern FILE *yyin;
void *Malloc(unsigned n)
{ void *p;
    if (!(p = malloc(n)))
    {
        fprintf(stderr, "Malloc(%d) failed.\n", n);
        fflush(stderr);
        fclose(yyin);
        abort();
    }
    return p;
}
