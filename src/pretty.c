#include <stdio.h>
#include "pretty.h" 

int indentLevel = 0;
void idt(){for(int i=0;i<indentLevel;i++){printf("    ");}}
void nl(){printf("\n");}


void prettyPROG(PROG *p) {
    printf("package ");
    printf("%s\n",p->packagename);
    prettyDECL(p->topdcls);
}

void prettyDECL(DECL *d) {
    if (d) {
        switch(d->kind) {
            case VAR_DECL:
                printf("var ");
                break;
            case TYPE_DECL:
                printf("type ");
                break;
            case FUNC_DECL:
                printf("func ");
                break;
        }
        prettyPARA(d->specs);
        nl();
        prettyDECL(d->next);
    }
}

void prettyEXPR(EXPR *e) {
    if (e) {
        char *op = "";
        switch(e->kind) {
            case DEC_LITERAL:
                printf("%d", e->val.intLiteral);
                break;
            case OCT_LITERAL:
                printf("%o", e->val.intLiteral);
                break;
            case HEX_LITERAL:
                printf("%x", e->val.intLiteral);
                break;
            case FLOAT_LITERAL:
                printf("%f", e->val.floatLiteral);
                break;
            case RAW_LITERAL:
            case STRING_LITERAL:
            case RUNE_LITERAL:
            case IDENT_LITERAL:
                printf("%s", e->val.stringLiteral);
                break;
            case BOOL_LITERAL:
                if (e->val.boolLiteral) {
                    printf("true");
                } else {
                    printf("false");
                }
                break;
            case MEMBER_EXPR:
                prettyEXPR(e->val.member.lhs);
                printf(".%s", e->val.member.rhs);
                break;
            case INDEX_EXPR:
                prettyEXPR(e->val.binary.lhs);
                    printf("[");
                prettyEXPR(e->val.binary.rhs);
                    printf("]");
                break;
            case EQ_EXPR:
                op = "==";
                break;
            case NE_EXPR:
                op = "!=";
                break;
            case LT_EXPR:
                op = "<";
                break;
            case LE_EXPR:
                op = "<=";
                break;
            case GT_EXPR:
                op = ">";
                break;
            case GE_EXPR:
                op = "<=";
                break;
            case ADD_EXPR:
                op = "+";
                break;
            case MIN_EXPR:
                op = "-";
                break;
            case MUL_EXPR:
                op = "*";
                break;
            case DIV_EXPR:
                op = "/";
                break;
            case MOD_EXPR:
                op = "%";
                break;
            case AND_EXPR:
                op = "&&";
                break;
            case OR_EXPR:
                op = "||";
                break;
            case LOR_EXPR:
                op = "|";
                break;
            case LAND_EXPR:
                op = "&";
                break;
            case XOR_EXPR:
                op = "^";
                break;
            case NXOR_EXPR:
                op = "&^";
                break;
            case LSHIFT_EXPR:
                op = "<<";
                break;
            case RSHIFT_EXPR:
                op = ">>";
                break;
            case NOT_EXPR:
                op = "!";
                break;
            case UADD_EXPR:
                op = "+";
                break;
            case UMIN_EXPR:
                op = "-";
                break;
            case UXOR_EXPR:
                op = "^";
                break;
            case FUNC_EXPR:
                printf("%s(",e->val.func.name);
                prettyEXPR(e->val.func.args);
                printf(")");
                break;
            case LIST_EXPR:
                prettyEXPR(e->val.unary);
                //prettyEXPR(e->next);
                /*
                prettyEXPR(e->next);
                if (e->next) {
                    if (e->next->next) {
                        printf(", ");
                    }
                }
                */
                break;
        }
        if (op && op[0] != '\0') {
            printf("(");
            prettyEXPR(e->val.binary.lhs);
            printf("%s",op);
            prettyEXPR(e->val.binary.rhs);
            printf(")");
        }
    }
}

void prettyTYPE(TYPE *t) {
    if (t) {
        printf(" ");
        switch(t->kind) {
            case IDENT_TYPE:
                printf("%s", t->val.identtype);
                break;
            case STRUCT_TYPE:
                printf("struct {\n");
                prettyPARA(t->val.field);
                printf("}\n");
                break;
            case SLICE_TYPE:
                printf("[]");
                prettyTYPE(t->val.array.next);
                break;
            case ARRAY_TYPE:
                printf("[%d]", t->val.array.size);
                prettyTYPE(t->val.array.next);
                break;
        }

    }
}

void prettyPARA(PARA *p) {
    if (p) {
        EXPR *n = p->expr;
        EXPR *a = p->rhs;
        switch(p->kind) {
            case VAR_TYPE_PARA:
                do {
                    prettyEXPR(n);
                    if (n->next) {
                        printf(", ");
                    }
                } while ((n = n->next));
                prettyTYPE(p->type);
                break;
            case VAR_ASSIGN_PARA:
                do {
                    prettyEXPR(n);
                    if (n->next) {
                        printf(", ");
                    }
                } while ((n = n->next));
                printf(" = ");
                do {
                    prettyEXPR(a);
                    if (a->next) {
                        printf(", ");
                    }
                } while ((a = a->next));
                break;
            case VAR_TYPE_ASSIGN_PARA:
                do {
                    prettyEXPR(n);
                    if (n->next) {
                        printf(", ");
                    }
                } while ((n = n->next));
                prettyTYPE(p->type);
                printf(" = ");
                do {
                    prettyEXPR(a);
                    if (a->next) {
                        printf(", ");
                    }
                } while ((a = a->next));
                break;
            case FUNC_PARA:
                prettyEXPR(p->expr);
                printf("(");
                prettyPARA(p->funcpara.args);
                printf(")");
                prettyTYPE(p->type);
                prettySTMT(p->funcpara.block);
                break;
        }
    }
}

void prettySTMT(STMT *s) {
    if (s) {
        switch(s->kind) {
            case LIST_STMT:
                idt(); printf("\n");
                idt(); prettySTMT(s->val.stmt);
                break;
            case ASSIGN_STMT:
                prettyEXPR(s->val.assign.lhs);
                printf(" = ");
                prettyEXPR(s->val.assign.rhs);
                printf("; ");
                break;
            case AADD_STMT:
                prettyEXPR(s->val.assign.lhs);
                printf(" += ");
                prettyEXPR(s->val.assign.rhs);
                printf("; ");
                break;
            case AMIN_STMT:
                prettyEXPR(s->val.assign.lhs);
                printf(" -= ");
                prettyEXPR(s->val.assign.rhs);
                printf("; ");
                break;
            case AMUL_STMT:
                prettyEXPR(s->val.assign.lhs);
                printf(" *= ");
                prettyEXPR(s->val.assign.rhs);
                printf("; ");
                break;
            case ADIV_STMT:
                prettyEXPR(s->val.assign.lhs);
                printf(" /= ");
                prettyEXPR(s->val.assign.rhs);
                printf("; ");
                break;
            case AMOD_STMT:
                prettyEXPR(s->val.assign.lhs);
                printf(" %%= ");
                prettyEXPR(s->val.assign.rhs);
                printf("; ");
                break;
            case AOR_STMT:
                prettyEXPR(s->val.assign.lhs);
                printf(" |= ");
                prettyEXPR(s->val.assign.rhs);
                printf("; ");
                break;
            case AAND_STMT:
                prettyEXPR(s->val.assign.lhs);
                printf(" &= ");
                prettyEXPR(s->val.assign.rhs);
                printf("; ");
                break;
            case AXOR_STMT:
                prettyEXPR(s->val.assign.lhs);
                printf(" ^= ");
                prettyEXPR(s->val.assign.rhs);
                printf("; ");
                break;
            case ANXOR_STMT:
                prettyEXPR(s->val.assign.lhs);
                printf(" &^= ");
                prettyEXPR(s->val.assign.rhs);
                printf("; ");
                break;
            case ALSHIFT_STMT:
                prettyEXPR(s->val.assign.lhs);
                printf(" <<= ");
                prettyEXPR(s->val.assign.rhs);
                printf("; ");
                break;
            case ARSHIFT_STMT:
                prettyEXPR(s->val.assign.lhs);
                printf(" >>= ");
                prettyEXPR(s->val.assign.rhs);
                printf("; ");
                break;
            case SHORTDECL_STMT:
                prettyEXPR(s->val.assign.lhs);
                printf(" := ");
                prettyEXPR(s->val.assign.rhs);
                printf("; ");
                break;
            case VAR_DECL_STMT:
                printf("var ");
                prettyEXPR(s->val.assign.lhs);
                printf(" = ");
                prettyEXPR(s->val.assign.rhs);
                break;
            case TYPE_DECL_STMT:
                printf("type ");
                prettyEXPR(s->val.assign.lhs);
                printf(" = ");
                prettyEXPR(s->val.assign.rhs);
                break;
            case INCR_STMT:
                prettyEXPR(s->val.incdecstmt);
                printf("++");
                break;
            case DECR_STMT:
                prettyEXPR(s->val.incdecstmt);
                printf("--");
                break;
            case FOR_STMT:
                printf("for ");
                prettySTMT(s->val.forstmt.pre);
                prettyEXPR(s->val.forstmt.cond);
                prettySTMT(s->val.forstmt.post);
                prettySTMT(s->val.forstmt.block);
                break;
            case IF_STMT:
                printf("if ");
                prettySTMT(s->val.ifstmt.pre);
                prettyEXPR(s->val.ifstmt.cond);
                prettySTMT(s->val.ifstmt.block);
                break;
            case IF_ELIF_STMT:
                printf("if ");
                prettySTMT(s->val.ifelifstmt.pre);
                prettyEXPR(s->val.ifelifstmt.cond);
                prettySTMT(s->val.ifelifstmt.ifblock);
                idt(); printf("else "); prettySTMT(s->val.ifelifstmt.elifstmt);
                break;
            case IF_ELSE_STMT:
                printf("if ");
                prettySTMT(s->val.ifelsestmt.pre);
                prettyEXPR(s->val.ifelsestmt.cond);
                prettySTMT(s->val.ifelsestmt.ifblock);
                idt(); printf("else ");
                prettySTMT(s->val.ifelsestmt.elseblock);
                break;
            case BLOCK_STMT:
                nl(); idt(); printf("{");
                indentLevel++;
                prettySTMT(s->val.block);
                indentLevel--;
                nl(); idt(); printf("}"); nl();
                break;
            case PRINT_STMT:
                printf("printf(");
                prettyEXPR(s->val.printarg);
                printf(")");
                break;
            case PRINTLN_STMT:
                printf("println(");
                prettyEXPR(s->val.printarg);
                printf(")");
                break;
            case RETURN_STMT:
                printf("return ");
                prettyEXPR(s->val.returnval);
                break;
            case SWITCH_STMT:
                printf("switch ");
                prettySTMT(s->val.switchstmt.pre);
                printf("; ");
                prettyEXPR(s->val.switchstmt.test);
                printf(" {\n");
                prettyCASE(s->val.switchstmt.clause);
                idt(); printf("}\n");
                break;
            case BREAK_STMT:
                printf("break");
                break;
            case CONTINUE_STMT:
                printf("continue");
                break;
            case FUNC_STMT:
                prettyEXPR(s->val.func);
                break;
        }
        prettySTMT(s->next);
    }
}

void prettyCASE(CASE *c) {
    if (c) {
        switch(c->kind) {
            case CLAUSE_CASE:
                printf("case ");
                prettyEXPR(c->cond);
                printf(":");
                prettySTMT(c->body);
                nl();
            case DEFAULT_CASE:
                printf("default:");
                prettySTMT(c->body);
                nl();
        }
    }
}
