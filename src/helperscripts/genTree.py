import csv 
import sys 

nodeTypes = [] 

with open('tree_structure.csv', 'r') as f: 
	r = csv.reader(f)
	rows = list(r)

i = 1 
while i < len(rows)-1: 
	newNode = [] 
	newNode.append(rows[i])
	i = i + 1  
	while(rows[i][0] == '' and i < len(rows)-1): 
		newNode.append(rows[i])
		i = i + 1
	if(i == len(rows)-1): 
		newNode.append(rows[i])
	nodeTypes.append(newNode) 

treec = "" 
treeh_top = "" 
treeh_body = ""
treeh_bottom = ""

for node in nodeTypes: 
	structName = node[0][0].strip(' ').strip('*')
	fKind = 1 
	fName = 2 
	uKind = 3
	uName = 4
	union = [] 
	fields = []
	structs = {}  
	for row in node[1:]: 
		if(row[uName] != ''):
			union.append([row[uKind], row[uName]])
		elif(row[fName] != ''): 
			fields.append([row[fKind], row[fName]])
		if(row[uName+1] != ''):
			s = [] 
			for i in range(len(row[uName+1:])/2): 
				if(row[uName+1+i*2] == ''): 
					break
				s.append([row[uName+1+i*2], row[uName+1+i*2+1]])
			structs[row[uName]] = s

	##########
	# TREE.H #
	##########

	# top 
	treeh_top += "typedef struct %s %s;\n"%(structName, structName)

	# body  
	treeh_body += "struct %s {\n    "%structName
	for f in fields: 
		treeh_body += "%s %s;\n    "%(f[0], f[1])
	if(len(union)):
		treeh_body += "enum { "
		for u in union: 
			treeh_body += "%s_%s, "%(structName.title().replace('_',''), u[1].title()) 
		treeh_body = treeh_body [:-2]
		treeh_body += "} kind;\n    " 
		treeh_body += "union {\n        "
		for u in union: 
			if(u[0] == ''): 
				continue 
			treeh_body += "%s "%(u[0])
			if(u[0] == "struct"): 
				treeh_body += "{ "
				for s in structs[u[1]]:
					treeh_body += "%s %s, "%(s[0], s[1])
				treeh_body = treeh_body [:-2]
				treeh_body += "} "
			treeh_body += "%s;\n        "%(u[1]) 
		treeh_body = treeh_body [:-4]
		treeh_body += "} val;\n    " 
	treeh_body = treeh_body[:-4]
	treeh_body += "};\n\n"

	# bottom  
	if(len(union)): 
		for u in union: 
			treeh_bottom += "%s *make%s_%s ("%(structName, structName.replace('_',''), u[1].title())
			for f in fields: 
				treeh_bottom += "%s %s, "%(f[0], f[1])
			if(u[0] == "struct"): 
				for s in structs[u[1]]: 
					treeh_bottom += "%s %s, "%(s[0], s[1])
			else: 
				if(u[0] == ''): 
					treeh_bottom += ");\n" 
					continue 
				treeh_bottom += "%s %s, "%(u[0], u[1])
			treeh_bottom = treeh_bottom [:-2]
			treeh_bottom += ");\n"
	else: 
		treeh_bottom += "%s *make%s ("%(structName, structName.replace('_',''))
		for f in fields: 
			treeh_bottom += "%s %s, "%(f[0], f[1])
		treeh_bottom = treeh_bottom [:-2]
		treeh_bottom += "); \n"

	##########
	# TREE.C #
	##########

	n = structName.lower()[0]
	if(len(union)): 
		for u in union:

			# signature 
			treec += "%s *make%s_%s ("%(structName, structName.replace('_',''), u[1].title())
			for f in fields: 
				treec += "%s %s, "%(f[0], f[1])
			if(u[0] == "struct"): 
				for s in structs[u[1]]: 
					treec += "%s %s, "%(s[0], s[1])
			else: 
				if(u[0] == ''): 
					treec += ") {\n    " 
					treec += "%s *%s = malloc(sizeof(%s));\n    "%(structName, n, structName)
					treec += "%s->kind = %s_%s;\n    "%(n, structName.replace('_','').title(), u[1].title())
					treec += "return %s;"%n
					treec += "\n}\n\n"
					continue 
				treec += "%s %s, "%(u[0], u[1])
			treec = treec [:-2]
			treec += ") {\n    "

			# function body
			treec += "%s *%s = malloc(sizeof(%s));\n    "%(structName, n, structName)
			treec += "%s->kind = %s_%s;\n    "%(n, structName.replace('_','').title(), u[1].title().strip(' '))
			if(u[0] == "struct"): 
				for s in structs[u[1]]: 
					treec += "%s->val.%s.%s = %s;\n    "%(n, u[1].strip(' '), s[1].strip(' '), s[1].strip(' '))
			else: 
				for f in fields: 
					treec += "%s->%s = %s;\n    "%(n, f[1].strip(' '), f[1].strip(' '))
				treec += "%s->val.%s = %s;\n    "%(n, u[1].strip(' '), u[1].strip(' '))
			treec += "return %s;"%n
			treec += "\n}\n\n"
	else: 
		# signature 
		treec += "%s *make%s ("%(structName, structName.replace('_',''))
		for f in fields: 
			treec += "%s %s, "%(f[0], f[1].strip(' '))
		treec = treec [:-2]
		treec += ") {\n    "
		# function body 
		treec += "%s *%s = malloc(sizeof(%s));\n    "%(structName, n, structName)
		for f in fields: 
			treec += "%s->%s = %s; \n    "%(n, f[1].strip(' '), f[1].strip(' '))
		treec += "return %s;"%n
		treec += "\n}\n\n"

if(sys.argv[1] == "h"): 
	print(treeh_top)
	print(treeh_body)
	print(treeh_bottom)
elif(sys.argv[1] == "c"): 
	print(treec)
