# This script just makes adding operators to our scanner/parser a bit easier. The 'operators.txt' file was copied and pasted from the original scanner file.
# Edit the two variables below according to desired output. 
parser = True # print parser code                                                   
scanner = False  # print scanner code 

binary = ["\|\|", "&&", "==", "!=", "<", "<=", ">", ">=", "+", "-", "\|", "^", "*", "/", "%", "<<", ">>", "&", "&^"] 
unary = ["+","-", "!", "^", "*", "&", "<-"]
lines = []

binarystr = "BINARYOP ("
for item in binary:
    binarystr += "\"%s\" | "%item
binarystr += ")\n"
if(scanner): print(binarystr)

unarystr = "UNARYOP ("
for item in unary:
    unarystr += "\"%s\" | "%item
unarystr += ")\n"
if(scanner): print(unarystr)

with open("operators.txt") as f:
    for line in f.readlines():
        try: 
            lines.append(line.split('"')[1])
        except:
            pass

if(not parser and not scanner): print("Warning: nothing will be printed. Edit operators.py to print scanner or parser code.") 

for item in lines:
    if(item in unary and scanner): 
        print("\"%s\"\t{ char *tmp = strdup(yytext);\n\t  yylval.unaryop = tmp; \n\t  return tUNARYOP; }\n"%item)
    elif(item in binary and scanner): 
        print("\"%s\"\t{ char *tmp = strdup(yytext);\n\t  yylval.binaryop = tmp; \n\t  return tBINARYOP; }"%item)
    elif(item not in unary and item not in binary):
        if(scanner): 
            print("\"%s\"\t{ return \"%s\"; }"%(item,item))
        if(parser): 
            print("| exp \"%s\" exp { /*  */ }"%item)



