#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include "tree.h"
#include "pretty.h"
#include "weeder.h"

extern FILE *yyin;
int yylex();
void yyparse();
bool g_tokens = false; //TOKEN mode
PROG *root;

int main(int argc, char **argv)
{
    if (argc != 3)
    {
        fprintf(stderr, "Error: Wrong number of arguments (%d)\n", argc);
        return -1;
    }
    else
    {
        // Parse arguments
        enum MODE { SCAN, TOKENS, PARSE, PRETTY, SYMBOL, TYPECHECK, CODEGEN };

        char *modestr = argv[1];
        char *filename = argv[2];
        enum MODE mode;
        bool OK = false; // true to output "OK"

        if (strcmp(modestr, "scan") == 0) { mode = SCAN; OK = true; }
        else if (strcmp(modestr, "tokens") == 0) { mode = TOKENS; g_tokens = true;}
        else if (strcmp(modestr, "parse") == 0) { mode = PARSE; OK = true; }
        else if (strcmp(modestr, "pretty") == 0) { mode = PRETTY; }
        else if (strcmp(modestr, "symbol") == 0) { mode = SYMBOL; }
        else if (strcmp(modestr, "typecheck") == 0) { mode = TYPECHECK; OK = true; }
        else if (strcmp(modestr, "codegen") == 0) { mode = CODEGEN; OK = true; }
        else
        {
            fprintf(stderr, "Error: Unrecognized mode (%s)\n", modestr);
            return -1;
        }

        if ((yyin = fopen(filename, "r")) != NULL)
        {
            switch (mode)
            {
                case SCAN:
                case TOKENS:;
                    while (yylex()) {}
                    break;
                case PARSE:
                case PRETTY:
                case SYMBOL:
                case TYPECHECK:
                case CODEGEN:
                    yyparse();
                    weederPROG(root);
                break;
            }
            if (OK) { printf("OK\n"); }
            else if (mode == PRETTY)
            {
                prettyPROG(root);
            }
            fclose(yyin);
        }
        else
        {
            fprintf(stderr, "Error: Unable to open file (%s)\n", filename);
            return -1;
        }
    }
    return 0;
}
