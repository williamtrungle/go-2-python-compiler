%{
#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include "error.h"
#include "parser.h"
extern bool g_tokens;

int yylex();

int eof = 0;
bool multi_line_comment = false; 
int lastToken = -1;
#define RETURN(token) lastToken = token; return token;
int needsSemiColon()
{
	return lastToken == tIDENTIFIER ||
	lastToken == tDEC ||
	lastToken == tOCT ||
	lastToken == tHEX ||
	lastToken == tFLOAT ||
	lastToken == tRUNE ||
	lastToken == tSTRING ||
	lastToken == tBOOL ||
	lastToken == tRAWSTRING ||
	lastToken == tBREAK ||
	lastToken == tCONT ||
	lastToken == tFT ||
	lastToken == tRET ||
	lastToken == tINCR ||
	lastToken == tDECR ||
    lastToken == tRPAREN ||
    lastToken == tRBRACK ||
    lastToken == tRBRACE;
}
%}
%x comment
%option yylineno
%option noinput
%option nounput

ascii [-_=+()`~!@#$%^&*\[\]{};:|,<.>/?a-zA-Z0-9 ]
escapable \\[abfnrtv\\]

%%
[ \t\r]+

break         { if(g_tokens) { printf("tBREAK\n"); }      RETURN(tBREAK); }
case          { if(g_tokens) { printf("tCASE\n"); }       RETURN(tCASE); }
chan          { if(g_tokens) { printf("tCHAN\n"); }       RETURN(tCHAN); }
const         { if(g_tokens) { printf("tCONST\n"); }      RETURN(tCONST); }
continue      { if(g_tokens) { printf("tCONT\n"); }       RETURN(tCONT); }
default       { if(g_tokens) { printf("tDEFAULT\n"); }    RETURN(tDEFAULT); }
defer         { if(g_tokens) { printf("tDEFER\n"); }      RETURN(tDEFER); }
else          { if(g_tokens) { printf("tELSE\n"); }       RETURN(tELSE); }
fallthrough   { if(g_tokens) { printf("tFT\n"); }         RETURN(tFT); }
for           { if(g_tokens) { printf("tFOR\n"); }        RETURN(tFOR); }
func          { if(g_tokens) { printf("tFUNC\n"); }       RETURN(tFUNC); }
go            { if(g_tokens) { printf("tGO\n"); }         RETURN(tGO); }
goto          { if(g_tokens) { printf("tGOTO\n"); }       RETURN(tGOTO); }
if            { if(g_tokens) { printf("tIF\n"); }         RETURN(tIF); }
import        { if(g_tokens) { printf("tIMPORT\n"); }     RETURN(tIMPORT); }
interface     { if(g_tokens) { printf("tINTF\n"); }       RETURN(tINTF); }
map           { if(g_tokens) { printf("tMAP\n"); }        RETURN(tMAP); }
package       { if(g_tokens) { printf("tPKG\n"); }        RETURN(tPKG); }
range         { if(g_tokens) { printf("tRANGE\n"); }      RETURN(tRANGE); }
return        { if(g_tokens) { printf("tRET\n"); }        RETURN(tRET); }
select        { if(g_tokens) { printf("tSEL\n"); }        RETURN(tSEL); }
struct        { if(g_tokens) { printf("tSTRUCT\n"); }     RETURN(tSTRUCT); }
switch        { if(g_tokens) { printf("tSWITCH\n"); }     RETURN(tSWITCH); }
type          { if(g_tokens) { printf("tTYPE\n"); }       RETURN(tTYPE); }
var           { if(g_tokens) { printf("tVAR\n"); }        RETURN(tVAR); }
append        { if(g_tokens) { printf("tAPPEND\n"); }     RETURN(tAPPEND); }
print         { if(g_tokens) { printf("tPRINT\n"); }      RETURN(tPRINT); }
println       { if(g_tokens) { printf("tPRINTLN\n"); }    RETURN(tPRINTLN); }

([0-9]+"."[0-9]*)|([0-9]*"."[0-9]+) {
    if (g_tokens) {printf("tFLOAT(%s)\n", yytext); }
    yylval.floatval = atof(yytext);
    RETURN(tFLOAT);
}

0|[1-9][0-9]* {
    if (g_tokens) {printf("tDEC(%s)\n", yytext); }
    yylval.intval = strtol(yytext, NULL, 10); 
    RETURN(tDEC);
}

0[0-7]+ {
    if (g_tokens) {printf("tOCT(%s)\n", yytext); }
    yylval.intval = strtol(yytext, NULL, 8); 
    RETURN(tOCT);
}

0[xX][0-9a-fA-F]+ {
    if (g_tokens) {printf("tHEX(%s)\n", yytext); }
    yylval.intval = strtol(yytext, NULL, 16); 
    RETURN(tHEX);
}

true|false {
    if (g_tokens) { printf("tBOOL(%s)\n", yytext) ; }
    if (strcmp(yytext, "true") == 0) { yylval.boolval = true; }
    else { yylval.boolval = false; }
    RETURN(tBOOL);
}

'({ascii}|{escapable}|\"|(\\'))' {
    if (g_tokens) {printf("tRUNE(%s)\n", yytext); }
    yylval.stringval = strdup(yytext); 
    RETURN(tRUNE);
}

`[^`]*` {
    char temp[yyleng];
    char c;
    int t = 0;
    for (int y = 0; y < yyleng; y++) {
        c = *(yytext + y);
        if (c != '\r')
        {
            temp[t++] = c;
        }
    }
    temp[t] = '\0';
    if (g_tokens) { printf("tRAWSTRING(%s)\n", temp); }
    yylval.stringval = strdup(temp);
    RETURN(tRAWSTRING);
}

\"({ascii}|{escapable}|'|(\\\"))*\" {
    if (g_tokens) { printf("tSTRING(%s)\n", yytext); }
    yylval.stringval = strdup(yytext);
    RETURN(tSTRING);
}  

[_a-zA-Z][_a-zA-Z0-9]* {
    if (g_tokens) { printf("tIDENTIFIER(%s)\n", yytext); }
    yylval.id = strdup(yytext);
    RETURN(tIDENTIFIER);
} 

"&^="   { if (g_tokens) { printf("&^=\n"); }  RETURN(tANXOR); }
"=="    { if (g_tokens) { printf("==\n"); }   RETURN(tEQ); }
"!="    { if (g_tokens) { printf("!=\n"); }   RETURN(tNE); }
"<"     { if (g_tokens) { printf("<\n"); }    RETURN(tLT); }
"<="    { if (g_tokens) { printf("<=\n"); }   RETURN(tLE); }
">"     { if (g_tokens) { printf(">\n"); }    RETURN(tGT); }
">="    { if (g_tokens) { printf(">=\n"); }   RETURN(tGE); }
"+"     { if (g_tokens) { printf("+\n"); }    RETURN(tADD); }
"-"     { if (g_tokens) { printf("-\n"); }    RETURN(tMIN); }
"*"     { if (g_tokens) { printf("*\n"); }    RETURN(tMUL); }
"/"     { if (g_tokens) { printf("/\n"); }    RETURN(tDIV); }
"%"     { if (g_tokens) { printf("%%\n"); }   RETURN(tMOD); }
"||"    { if (g_tokens) { printf("||\n"); }   RETURN(tOR); }
"&&"    { if (g_tokens) { printf("&&\n"); }   RETURN(tAND); }
"="     { if (g_tokens) { printf("=\n"); }    RETURN(tASGN); }
":="    { if (g_tokens) { printf(":=\n"); }   RETURN(tSHRTASGN); }
"."     { if (g_tokens) { printf(".\n"); }    RETURN(tMEMBER); }
"+="    { if (g_tokens) { printf("+=\n"); }   RETURN(tAADD); }
"-="    { if (g_tokens) { printf("-=\n"); }   RETURN(tAMIN); }
"*="    { if (g_tokens) { printf("*=\n"); }   RETURN(tAMUL); }
"/="    { if (g_tokens) { printf("/=\n"); }   RETURN(tADIV); }
"%="    { if (g_tokens) { printf("%%=\n"); }  RETURN(tAMOD); }
"|="    { if (g_tokens) { printf("|=\n"); }   RETURN(tAOR); }
"&="    { if (g_tokens) { printf("&=\n"); }   RETURN(tAAND); }
"^="    { if (g_tokens) { printf("^=\n"); }   RETURN(tAXOR); }
"<<="   { if (g_tokens) { printf("<<=\n"); }  RETURN(tALSHIFT); }
">>="   { if (g_tokens) { printf(">>=\n"); }  RETURN(tARSHIFT); }
"|"     { if (g_tokens) { printf("|\n"); }    RETURN(tLOR); }
"&"     { if (g_tokens) { printf("&\n"); }    RETURN(tLAND); }
"^"     { if (g_tokens) { printf("^\n"); }    RETURN(tXOR); }
"&^"    { if (g_tokens) { printf("&^\n"); }   RETURN(tNXOR); }
"<<"    { if (g_tokens) { printf("<<\n"); }   RETURN(tLSHIFT); }
">>"    { if (g_tokens) { printf(">>\n"); }   RETURN(tRSHIFT); }
"<-"    { if (g_tokens) { printf("<-\n"); }   RETURN(tCHANOP); }
"!"     { if (g_tokens) { printf("!\n"); }    RETURN(tNOT); }
"++"    { if (g_tokens) { printf("++\n"); }   RETURN(tINCR); }
"--"    { if (g_tokens) { printf("--\n"); }   RETURN(tDECR); }
"("     { if (g_tokens) { printf("(\n"); }    RETURN(tLPAREN); }
")"     { if (g_tokens) { printf(")\n"); }    RETURN(tRPAREN); }
"{"     { if (g_tokens) { printf("{\n"); }    RETURN(tLBRACE); }
"}"     { if (g_tokens) { printf("}\n"); }    RETURN(tRBRACE); }
"["     { if (g_tokens) { printf("[\n"); }    RETURN(tLBRACK); }
"]"     { if (g_tokens) { printf("]\n"); }    RETURN(tRBRACK); }
"..."   { if (g_tokens) { printf("...\n"); }  RETURN(tELLIPSIS); }
":"     { if (g_tokens) { printf(":\n"); }    RETURN(tCOLON); }
";"     { if (g_tokens) { printf(";\n"); }    RETURN(tSEMIC); }
","     { if (g_tokens) { printf(",\n"); }    RETURN(tCOMMA); }

\n {
    if (needsSemiColon())
    {
        if (g_tokens) { printf(";\n"); }
        RETURN(tSEMIC);
    }
}

"//".*  { if (g_tokens) { printf("// comments\n"); } }

"/*" BEGIN(comment);
<comment>[^*\n]*        /* ignore non asterisk or newline characters */
<comment>"*"+[^*/\n]*   /* ignore any *'s without a '/' after */
<comment>\n             {/*++yylineno*/ multi_line_comment = true; }
<comment>"*"+"/" {
    if (g_tokens)
    {
        if (multi_line_comment)
        {
            printf("/* block \n comments multi */\n");
        }
        else
        {
            printf("/* block comment single */\n");
        }
    } 

    if (multi_line_comment)
    {
        if (needsSemiColon())
        {
            if (g_tokens) { printf(";\n"); }
            multi_line_comment = false; 
            BEGIN(INITIAL);
            RETURN(tSEMIC);
        }
    }
    multi_line_comment = false; 
    BEGIN(INITIAL);
}

<<EOF>> {
    if (eof)
    {
        return 0;
    }
    else
    {
        eof++;
        if (needsSemiColon())
        {
            if (g_tokens) { printf(";\n"); }
            RETURN(tSEMIC);
        }
        else
        {
            return 0;
        }
    }
}

. return error(yylineno, yytext);
%%
