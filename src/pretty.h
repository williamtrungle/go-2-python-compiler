#ifndef PRETTY_H
#define PRETTY_H
#include "tree.h"
void prettyPROG(PROG *p); 
void prettyDECL(DECL *d);
void prettyEXPR(EXPR *e);
void prettyTYPE(TYPE *t);
void prettyPARA(PARA *p);
void prettySTMT(STMT *s);
void prettyCASE(CASE *c); 
#endif
