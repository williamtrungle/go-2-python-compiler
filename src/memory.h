#ifndef MEMORY_H
#define MEMORY_H
#define NEW(type) (type*)Malloc(sizeof(type))
void *Malloc(unsigned n);
#endif
