#include <stdlib.h>
#include "tree.h"
#include "memory.h"

PROG *makePROG(char *pn, DECL *td)
{ PROG *r;
    r = NEW(PROG);
    r->packagename = pn;
    r->topdcls = td;
    return r;
} 

// ----------------------------------------------------------------------------
// Declarations

DECL *makeDECL_vdcl(DECL *next, PARA *specs)
{ DECL *r;
    r = NEW(DECL);
    r->kind = VAR_DECL;
    r->next = next;
    r->specs = specs;
    return r;
}

DECL *makeDECL_tdcl(DECL *next, PARA *specs)
{ DECL *r;
    r = NEW(DECL);
    r->kind = TYPE_DECL;
    r->next = next;
    r->specs = specs;
    return r;
}

DECL *makeDECL_func(DECL *next, PARA *specs)
{ DECL *r;
    r = NEW(DECL);
    r->kind = FUNC_DECL;
    r->next = next;
    r->specs = specs;
    return r;
}

// ----------------------------------------------------------------------------
// Parameter lists

PARA *makePARA(PARA *next, ParaKind kind, EXPR *expr, TYPE *type)
{ PARA *r;
    r = NEW(PARA);
    r->next = next;
    r->kind = kind;
    r->expr = expr;
    r->type = type;
    r->rhs = NULL;
    return r;
}

PARA *makePARA_assign(PARA *next, ParaKind kind, EXPR *expr, TYPE *type, EXPR *rhs)
{ PARA *r;
    r = NEW(PARA);
    r->next = next;
    r->kind = kind;
    r->expr = expr;
    r->type = type;
    r->rhs = rhs;
    return r;
}

PARA *makePARA_func(EXPR *name, PARA *args, TYPE *returntype, STMT *block)
{ PARA *r;
    r = NEW(PARA);
    r->next = NULL;
    r->kind = FUNC_PARA;
    r->expr = name;
    r->type = returntype;
    r->funcpara.args = args;
    r->funcpara.block = block;
    return r;
}
// ----------------------------------------------------------------------------
// Types

TYPE *makeTYPE_ident(char *identtype)
{ TYPE *r;
    r = NEW(TYPE);
    r->kind = IDENT_TYPE;
    r->val.identtype = identtype;
    return r;
}

TYPE *makeTYPE_struct(PARA *field)
{ TYPE *r;
    r = NEW(TYPE);
    r->kind = STRUCT_TYPE;
    r->val.field = field;
    return r;
}

TYPE *makeTYPE_slice(TYPE *next)
{ TYPE *r;
    r = NEW(TYPE);
    r->kind = SLICE_TYPE;
    r->val.slice.next = next;
    return r;
}

TYPE *makeTYPE_array(int size, TYPE *next)
{ TYPE *r;
    r = NEW(TYPE);
    r->kind = ARRAY_TYPE;
    r->val.array.size = size;
    r->val.array.next = next;
    return r;
}


// ----------------------------------------------------------------------------
// Expressions

EXPR *makeEXPR_BinaryEXPR(EXPR *next, ExprKind kind, EXPR *lhs, EXPR *rhs)
{ EXPR *r;
    r = NEW(EXPR);
    r->kind = kind;
    r->next = next;
    r->val.binary.lhs = lhs;
    r->val.binary.rhs = rhs;
    return r;
}

EXPR *makeEXPR_UnaryEXPR(EXPR *next, ExprKind kind, EXPR *unary)
{ EXPR *r;
    r = NEW(EXPR);
    r->kind = kind;
    r->next = next;
    r->val.unary = unary;
    return r;
}

EXPR *makeEXPR_identifier(EXPR *next, char *id)
{ EXPR *r;
    r = NEW(EXPR);
    r->kind = IDENT_LITERAL;
    r->next = next;
    r->val.stringLiteral = id;
    return r;
}

EXPR *makeEXPR_floatLiteral(EXPR *next, float f)
{ EXPR *r;
    r = NEW(EXPR);
    r->kind = FLOAT_LITERAL;
    r->next = next;
    r->val.floatLiteral = f;
    return r;
}

EXPR *makeEXPR_stringLiteral(EXPR *next, char *s)
{ EXPR *r;
    r = NEW(EXPR);
    r->kind = STRING_LITERAL;
    r->next = next;
    r->val.stringLiteral = s;
    return r;
}

EXPR *makeEXPR_rawLiteral(EXPR *next, char *s)
{ EXPR *r;
    r = NEW(EXPR);
    r->kind = RAW_LITERAL;
    r->next = next;
    r->val.stringLiteral = s;
    return r;
}

EXPR *makeEXPR_runeLiteral(EXPR *next, char *s)
{ EXPR *r;
    r = NEW(EXPR);
    r->kind = RUNE_LITERAL;
    r->next = next;
    r->val.stringLiteral = s;
    return r;
}
 
EXPR *makeEXPR_decLiteral(EXPR *next, int i)
{ EXPR *r;
    r = NEW(EXPR);
    r->kind = DEC_LITERAL;;
    r->next = next;
    r->val.intLiteral = i;
    return r;
}

EXPR *makeEXPR_octLiteral(EXPR *next, int i)
{ EXPR *r;
    r = NEW(EXPR);
    r->kind = OCT_LITERAL;;
    r->next = next;
    r->val.intLiteral = i;
    return r;
}

EXPR *makeEXPR_hexLiteral(EXPR *next, int i)
{ EXPR *r;
    r = NEW(EXPR);
    r->kind = HEX_LITERAL;;
    r->next = next;
    r->val.intLiteral = i;
    return r;
}

EXPR *makeEXPR_boolLiteral(EXPR *next, bool b)
{ EXPR *r;
    r = NEW(EXPR);
    r->kind = BOOL_LITERAL;;
    r->next = next;
    r->val.boolLiteral = b;
    return r;
}

EXPR *makeEXPR_func(EXPR *next, char *name, EXPR *args)
{ EXPR *r;
    r = NEW(EXPR);
    r->kind = FUNC_EXPR;
    r->val.func.name = name;
    r->val.func.args = args;
    return r;
}

EXPR *makeEXPR_member(EXPR *next, EXPR* lhs, char* rhs)
{ EXPR *r;
    r = NEW(EXPR);
    r->kind = MEMBER_EXPR;
    r->next = next;
    r->val.member.lhs = lhs;
    r->val.member.rhs = rhs;
    return r;
}

// ----------------------------------------------------------------------------
// Statements

STMT *makeSTMT_LIST(STMT *next, STMT *stmt)
{ STMT *r;
    r = NEW(STMT);
    r->next = next;
    r->kind = LIST_STMT;
    r->val.stmt = stmt;
    return r;
}

STMT *makeSTMT_BLOCK(STMT* block)
{ STMT *r;
    r = NEW(STMT);
    r->kind = BLOCK_STMT;
    r->val.block = block;
    return r;
}

STMT *makeSTMT_ASSIGN(StmtKind kind, EXPR *lhs, EXPR *rhs)
{ STMT *r;
    r = NEW(STMT);
    r->kind = kind;
    r->val.assign.lhs = lhs;
    r->val.assign.rhs = rhs;
    return r;
}

STMT *makeSTMT_VDECL(PARA *specs)
{ STMT *r;
    r = NEW(STMT);
    r->kind = VAR_DECL_STMT;
    r->val.specs = specs;
    return r;
}

STMT *makeSTMT_TDECL(PARA *specs)
{ STMT *r;
    r = NEW(STMT);
    r->kind = TYPE_DECL_STMT;
    r->val.specs = specs;
    return r;
}

STMT *makeSTMT_INC(EXPR *incdecstmt)
{ STMT *r;
    r = NEW(STMT);
    r->kind = INCR_STMT;
    r->val.incdecstmt = incdecstmt;
    return r;
}

STMT *makeSTMT_DEC(EXPR *incdecstmt)
{ STMT *r;
    r = NEW(STMT);
    r->kind = DECR_STMT;
    r->val.incdecstmt = incdecstmt;
    return r;
}

STMT *makeSTMT_FOR(STMT *pre, EXPR *cond, STMT *post, STMT *block)
{ STMT *r;
    r = NEW(STMT);
    r->kind = FOR_STMT;
    r->val.forstmt.pre = pre;
    r->val.forstmt.cond = cond;
    r->val.forstmt.post = post;
    r->val.forstmt.block = block;
    return r;
}

STMT *makeSTMT_IF(STMT *pre, EXPR *cond, STMT *block)
{ STMT *r;
    r = NEW(STMT);
    r->kind = IF_STMT;
    r->val.ifstmt.pre = pre;
    r->val.ifstmt.cond = cond;
    r->val.ifstmt.block = block;
    return r;
}

STMT *makeSTMT_IFELSE(STMT *pre, EXPR *cond, STMT *ifblock, STMT *elseblock)
{ STMT *r;
    r = NEW(STMT);
    r->kind = IF_ELSE_STMT;
    r->val.ifelsestmt.pre = pre;
    r->val.ifelsestmt.cond = cond;
    r->val.ifelsestmt.ifblock = ifblock;
    r->val.ifelsestmt.elseblock = elseblock;
    return r;
}

STMT *makeSTMT_IFELIF(STMT *pre, EXPR *cond, STMT *ifblock, STMT *elifstmt)
{ STMT *r;
    r = NEW(STMT);
    r->kind = IF_ELIF_STMT;
    r->val.ifelifstmt.pre = pre;
    r->val.ifelifstmt.cond = cond;
    r->val.ifelifstmt.ifblock = ifblock;
    r->val.ifelifstmt.elifstmt = elifstmt;
    return r;
}
 
STMT *makeSTMT_PRINT(EXPR *printarg)
{ STMT *r;
    r = NEW(STMT);
    r->kind = PRINT_STMT;
    r->val.printarg = printarg;
    return r;
}

STMT *makeSTMT_PRINTLN(EXPR *printarg)
{ STMT *r;
    r = NEW(STMT);
    r->kind = PRINTLN_STMT;
    r->val.printarg = printarg;
    return r;
}
 
STMT *makeSTMT_RETURN(EXPR *returnval)
{ STMT *r;
    r = NEW(STMT);
    r->kind = RETURN_STMT;
    r->val.returnval = returnval;
    return r;
}

STMT *makeSTMT_SWITCH(STMT *pre, EXPR *test, CASE *clause)
{ STMT *r;
    r = NEW(STMT);
    r->kind = SWITCH_STMT;
    r->val.switchstmt.pre = pre;
    r->val.switchstmt.test = test;
    r->val.switchstmt.clause = clause;
    return r;
}

STMT *makeSTMT_CONT()
{ STMT *r;
    r = NEW(STMT);
    r->kind = CONTINUE_STMT;
    return r;
}

STMT *makeSTMT_BREAK()
{ STMT *r;
    r = NEW(STMT);
    r->kind = BREAK_STMT;
    return r;
}

STMT *makeSTMT_FUNC(EXPR* func)
{ STMT *r;
    r = NEW(STMT);
    r->kind = FUNC_STMT;
    r->val.func = func;
    return r;
}

// ----------------------------------------------------------------------------
// Switch case

CASE *makeCASE(CASE *next, CaseKind kind, EXPR *cond, STMT *body)
{ CASE *r;
    r = NEW(CASE);
    r->next = next;
    r->kind = kind;
    r->cond = cond;
    r->body = body;
    return r;
}
