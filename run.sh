#!/bin/bash

# Usage: ./run.sh <mode> <file>
# 	mode: scan|tokens|parse|pretty|symbol|typecheck|codegen
                                
# Check the command-line arguments are valid

if [ $# -lt 2 ]
then
	echo "Missing arguments"
	echo "Usage: $0 <mode> <file>"
	echo " + mode: scan|tokens|parse|pretty|symbol|typecheck|codegen"
	echo " + file: path to file (absolute or relative)"
	exit 1
fi

if [[ "|scan|tokens|parse|pretty|symbol|typecheck|codegen|" != *"|$1|"* ]]
then
	echo "Unknown mode \"$1\""
	echo "Usage: $0 <mode> <file>"
	echo " + mode: scan|tokens|parse|pretty|symbol|typecheck|codegen"
	echo " + file: path to file (absolute or relative)"
	exit 1

fi

# Invoke the compiler with the provided arguments: mode ($1) and file ($2)
#
# You MUST replace the following command with the command for invoking your compiler

if [ -d "$2" ]; then
    TOTAL="$(ls -1 $2 | wc -l)"
    MODE="${2##*/}"
    let "OK=0"
    for FILE in $(find "$2" -type f ! -name ".*")
    do
        if [[ "$MODE" == "valid" ]]; then
            if [[ "$(./src/compiler "$1" "$FILE")" == "OK" ]]; then
                let "OK++"
            else
                echo "${FILE##*/}"
            fi
        else
            if [[ "$(./src/compiler "$1" "$FILE" 2>/dev/null)" != "OK" ]]; then
                let "OK++"
            else
                echo "No error: ${FILE##*/}"
            fi
        fi
    done
    echo "$OK/$TOTAL"
else
    ./src/compiler "$1" "$2"
fi
