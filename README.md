# GoLite Compiler 2018
## Authors
- William Trung Le: 260239181
- Ashique Hossain: 260572615
- Shammamah Hossain (left)

## Phase 1 - Scanning/Parsing
The valid programs are the following:
- travellingsalesman.go
- higherOrder.go
- mergesort.go
- prime.go
